<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Report_m extends MY_Model {

	protected $_table_name  = 'msit_tb_report';
	protected $_primary_key = 'reportsID';
	protected $_order_by    = "reportsID asc";

	function __construct() {
		parent::__construct();
	}

	public function get_reports($array=NULL, $single=FALSE) {
		return parent::get($array, $single);
	}

	public function get_order_by_reports($warray=NULL, $array=NULL, $single=FALSE) {
		$query = parent::get_order_by($warray, $array, $single);
		return $query;
	}

	public function get_single_reports($warray=NULL, $array=NULL, $single=TRUE) {
		$query = parent::get_single($warray, $array, $single);
		return $query;
	}

	public function insert_reports($array) {
		$error = parent::insert($array);
		return TRUE;
	}

	public function update_reports($data, $id = NULL) {
		parent::update($data, $id);
		return $id;
	}

	public function delete_reports($id){
		parent::delete($id);
		return TRUE;
	}

	// customization 

	public function member_report($membershipID,$status){
		$query = $this->db->get_where('msit_tb_member',array('member_membership_type' => $membershipID, 'member_status' => $status));
		return $query->result();
	}	

	public function book_report($categoriesID,$status){
		$query = $this->db->get_where('msit_tb_book',array('book_category' => $categoriesID, 'book_status' => $status));
		return $query->result();
	}


	public function payment_report($for,$fromdate,$todate){
		if ($for != '0'){
			$this->db->where('payment_by', $for);
			$this->db->where('create_date >=', $fromdate);
			$this->db->where('create_date <=', $todate);
			$query =  $this->db->get('msit_tb_payment');
			return $query->result();
		}else{
			$this->db->where('create_date >=', $fromdate);
			$this->db->where('create_date <=', $todate);
			$query =  $this->db->get('msit_tb_payment');
			return $query->result();
		}
	}
}
