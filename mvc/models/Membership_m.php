<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Membership_m extends MY_Model {

	protected $_table_name  = 'msit_tb_membership';
	protected $_primary_key = 'membershipID';
	protected $_order_by    = "membershipID asc";

	function __construct() {
		parent::__construct();
	}

	public function get_membership($array=NULL, $single=FALSE) {
		return parent::get($array, $single);
	}

	public function get_order_by_membership($warray=NULL, $array=NULL, $single=FALSE) {
		$query = parent::get_order_by($warray, $array, $single);
		return $query;
	}

	public function get_single_membership($warray=NULL, $array=NULL, $single=TRUE) {
		$query = parent::get_single($warray, $array, $single);
		return $query;
	}

	public function insert_membership($array) {
		$error = parent::insert($array);
		return TRUE;
	}

	public function update_membership($data, $id = NULL) {
		parent::update($data, $id);
		return $id;
	}

	public function delete_membership($id){
		parent::delete($id);
		return TRUE;
	}
}
