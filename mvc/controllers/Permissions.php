<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Permissions extends Admin_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('usertype_m');
		$this->load->model('permissions_m');
		$this->load->model('permissionlog_m');
		$this->load->model('permissionmodule_m');
		// $lang = $this->session->get_userdata('language');
		$lang = settings()->language;
		$this->lang->load('permissions', $lang);
	}

	protected function rules() {
		$rules = array(
			array(
				'field'=> 'permissionsusertypeID',
				'label'=> 'Permissions UsertypeID',
				'rules'=> 'trim|required|numeric',
				)
			);
		return $rules;
	}

	public function index() {
		$usertypeID = escapeString($this->uri->segment('3'));

		if((int)$usertypeID) {
			$this->data['urlusertypeID'] = $usertypeID;
		} else {
			$this->data['urlusertypeID'] = '0';
		}

		$this->data['headerassets'] = array(
			'css' => array(
				'assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css',
				'assets/custom/css/hidetable.css'
				),
			'js' => array(
				'assets/bower_components/datatables.net/js/jquery.dataTables.min.js',
				'assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js',
				'assets/pages_js/permissions.js'
				)
			);

		if ($this->session->userdata('usertypeID') == '1') {
			$usertypes = $this->usertype_m->get_usertype(array('usertypeID','usertype'));
		}else {
			$usertypes = $this->usertype_m->get_order_by_usertype(array('usertypeID !=' => '1','usertype'));
		}

		$this->data['usertypes']   = $usertypes;
		$permissionlogs = $this->permissionlog_m->get_order_by_permissionlog(array('active'=>'yes'));

		$this->data['permissionmodules'] = pluck($this->permissionmodule_m->get_order_by_permissionmodule(array('active'=>1)),'name','permissionmoduleID');

		$permissionlogsArray = [];
		$permissionsModuleArray = [];
		if(inicompute($permissionlogs)) {
			foreach ($permissionlogs as $permissionlog) {
				if((strpos($permissionlog->name, '_add') == FALSE) && (strpos($permissionlog->name, '_edit') == FALSE) && (strpos($permissionlog->name, '_view') == FALSE) && (strpos($permissionlog->name, '_delete') == FALSE)) {
					$permissionsModuleArray[$permissionlog->permissionmoduleID][$permissionlog->permissionlogID] = $permissionlog;
				}
				$permissionlogsArray[$permissionlog->name] = $permissionlog->permissionlogID;
			}
		}

		$this->data['permissions'] = pluck_multi_array_key($this->permissions_m->get_permissions(),'permissionlogID','usertypeID','permissionlogID');

		$this->data['permissionlogsArray'] = $permissionlogsArray;
		$this->data['permissionsModuleArray']  = $permissionsModuleArray;
		$this->data["subview"] = "permissions/index";
		$this->load->view('_main_layout', $this->data);
	}

	public function save() {
		if($_POST) {
			$rules = $this->rules();
			$this->form_validation->set_rules($rules);
			if ($this->form_validation->run() == FALSE) {
				redirect("permissions/index",'refresh');
			} else {
				$permissionsusertypeID = $_POST['permissionsusertypeID'];
				unset($_POST['permissionsusertypeID']);
				
				$permissionArray = [];
				if(inicompute($_POST)) {
					$i = 0;
					foreach ($_POST as $permissionname => $permissionlogID) {
						$permissionArray[$i]['usertypeID']      = $permissionsusertypeID;
						$permissionArray[$i]['permissionlogID'] = $permissionlogID;
						$i++;
					}
				}

				if(inicompute($permissionArray)) {
					$this->permissions_m->delete_permissions_by_usertypeID($permissionsusertypeID);
					$this->permissions_m->insert_batch_permissions($permissionArray);
				}
				$this->session->set_flashdata('message', 'Message');
				redirect("permissions/index/$permissionsusertypeID",'refresh');
			}
		} else {
			
			redirect('permissions/index','refresh');
		}
	}

}
