<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Membership extends Admin_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('membership_m');
		$lang = settings()->language;
		$this->lang->load('membership', $lang);
	}

	public function index() {
		$this->data['headerassets'] = array(
			'css' => array(
				'assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css',
				'assets/custom/css/hidetable.css',
				),
			'js' => array(
				'assets/bower_components/datatables.net/js/jquery.dataTables.min.js',
				'assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js',
				'assets/pages_js/membership.js',
				)
			);
		$this->data['memberships'] = $this->membership_m->get_membership();
		$this->data['title'] = 'Member Type';
		$this->data["subview"] = "membership/index";
		$this->load->view('_main_layout', $this->data);
	}

	protected function rules() {
		$rules = array(
			array(
				'field'=> 'membership_name',
				'label'=> $this->lang->line('membership_name'),
				'rules'=> 'trim|required|max_length[60]',
				),
			array(
				'field'=> 'limit_book',
				'label'=> $this->lang->line('membership_limit_book'),
				'rules'=> 'trim|required|max_length[11]|required_no_zero',
				),
			array(
				'field'=> 'limit_day',
				'label'=> $this->lang->line('membership_limit_day'),
				'rules'=> 'trim|required|max_length[11]|required_no_zero',
				),
			array(
				'field'=> 'renew_limit',
				'label'=> $this->lang->line('membership_renew_limit'),
				'rules'=> 'trim|required|max_length[11]|required_no_zero',
				)
			);
		return $rules;
	}

	public function add(){
		if($_POST) {
			$this->form_validation->set_rules($this->rules());
			if($this->form_validation->run() == FALSE) {
				$validation_errors = $this->form_validation->verror_array();
				$json = array("confirmation" => 'error', 'validations' => $validation_errors);
				header("Content-Type: application/json", true);
				echo json_encode($json);
				exit;
			} else {

				$array = array(
					'membership_code'    		=> $this->input->post('membership_code'),
					'membership_name'    		=> $this->input->post('membership_name'),
					'membership_books_limit'  	=> $this->input->post('limit_book'),
					'membership_days_limit'  	=> $this->input->post('limit_day'),
					'membership_fee'  			=> $this->input->post('fee'),
					'penalty_fee'  				=> $this->input->post('penalty_fee'),
					'renew_limit'  				=> $this->input->post('renew_limit'),
					'membership_status'  		=> '1',
					'create_date'      			=> date('Y-m-d H:i:s'),
					'create_userID'    			=> $this->session->userdata('userID'),
					'create_usertypeID'			=> $this->session->userdata('usertypeID'),
					'modify_date'      			=> date('Y-m-d H:i:s'),
					'modify_userID'    			=> $this->session->userdata('userID'),
					'modify_usertypeID'			=> $this->session->userdata('usertypeID'),
				);
				$this->membership_m->insert_membership($array);
				$json = array("confirmation" => 'Success');
				header("Content-Type: application/json", true);
				echo json_encode($json);
			}
		} else {
			redirect(base_url('membership'));
		}
	}


	public function edit(){
		if($_POST) {
			$this->form_validation->set_rules($this->rules());
			if($this->form_validation->run() == FALSE) {
				$validation_errors = $this->form_validation->verror_array();
				$json = array("confirmation" => 'error', 'validations' => $validation_errors);
				header("Content-Type: application/json", true);
				echo json_encode($json);
				exit;
			} else {
				$array = array(
					'membership_name'    		=> $this->input->post('membership_name'),
					'membership_books_limit'  	=> $this->input->post('limit_book'),
					'membership_days_limit'  	=> $this->input->post('limit_day'),
					'membership_fee'  			=> $this->input->post('fee'),
					'penalty_fee'  				=> $this->input->post('penalty_fee'),
					'renew_limit'  				=> $this->input->post('renew_limit'),
					'modify_date'      			=> date('Y-m-d H:i:s'),
					'modify_userID'    			=> $this->session->userdata('userID'),
					'modify_usertypeID'			=> $this->session->userdata('usertypeID'),
				);
				$this->membership_m->update_membership($array, $this->input->post('id'));
				$json = array("confirmation" => 'Success');
				header("Content-Type: application/json", true);
				echo json_encode($json);
			} 
		}else {
			redirect(base_url('expenses'));
		}
	}

	public function retrive() {
		$membershipID = $this->input->post('id');
		if((int)$membershipID){
			$get_data = $this->membership_m->get_single_membership(array('membershipID' => $membershipID));
			if(inicompute($get_data)){
				$json = array(
					"confirmation" => 'Success',
					'id' => $get_data->membershipID, 
					'membership_name' 		 => $get_data->membership_name,
					'membership_books_limit' => $get_data->membership_books_limit,
					'membership_days_limit'  => $get_data->membership_days_limit,
					'membership_fee' 		 => $get_data->membership_fee,
					'penalty_fee' 		 	 => $get_data->penalty_fee,
					'renew_limit' 			 => $get_data->renew_limit,
					);
				header("Content-Type: application/json", true);
				echo json_encode($json);
				exit;
			} else {
				$json = array("confirmation" => 'error');
				header("Content-Type: application/json", true);
				echo json_encode($json);
			}
		}else {
			redirect(base_url('membership'));
		}
	}

	public function status(){
		$array = array(
			'membership_status'    => $this->input->post('status'),
			);
		$this->membership_m->update_membership($array, $this->input->post('id'));
		$json = array("confirmation" => 'Success');
		header("Content-Type: application/json", true);
		echo json_encode($json);
		
	}

	public function delete() {
		if($this->membership_m->delete_membership($this->input->post('id'))){
			$response['status']  = 'success';
			$response['message'] = 'Deleted Successfully ...';
			echo json_encode($response);
		}
	}
}
