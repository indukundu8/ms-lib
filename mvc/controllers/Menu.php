<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu extends Admin_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('menu_m');
		// $lang = $this->session->get_userdata('language');
		$lang = settings()->language;
		$this->lang->load('menu', $lang);
	}

	protected function rules() {
		$rules = array(
			array(
				'field'=> 'menuname',
				'label'=> 'Menu Name',
				'rules'=> 'trim|required|min_length[4]|max_length[128]',
			),
			array(
				'field'=> 'menulink',
				'label'=> 'Menu Link',
				'rules'=> 'trim|required|max_length[128]',
			),
			array(
				'field'=> 'menuicon',
				'label'=> 'Menu Icon',
				'rules'=> 'trim|required|min_length[4]|max_length[128]',
			),
			array(
				'field'=> 'priority',
				'label'=> 'Priority',
				'rules'=> 'trim|required|is_natural',
			),
			array(
				'field'=> 'parentmenuID',
				'label'=> 'Parent Menu',
				'rules'=> 'trim|required|is_natural',
			),
			array(
				'field'=> 'status',
				'label'=> 'Status',
				'rules'=> 'trim|required|is_natural_no_zero',
			)
		);
		return $rules;
	}

	public function index() {
		$this->data['headerassets'] = array(
			'css' => array(
				'assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css',
				'assets/custom/css/hidetable.css'
			),
			'js' => array(
				'assets/bower_components/datatables.net/js/jquery.dataTables.min.js',
				'assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js'
			)
		);
		$this->data['menus'] = $this->menu_m->get_menu();
		$this->data['menusArray'] = pluck($this->menu_m->get_menu(),'menuname','menuID');
		$this->data["subview"] = "menu/index";
		$this->load->view('_main_layout', $this->data);
	}

	public function add() {
		$this->data['parentmenus'] = $this->menu_m->get_order_by_menu(array('parentmenuID'=>0),array('menuID','menuname'));
		if($_POST) {
			$rules = $this->rules();
			$this->form_validation->set_rules($rules);
			if ($this->form_validation->run() == FALSE) {
				$this->data["subview"] = "menu/add";
				$this->load->view('_main_layout', $this->data);
			} else {
				$array = []; 
				$array['menuname']     = $this->input->post('menuname');
				$array['menulink']     = $this->input->post('menulink');
				$array['menuicon']     = $this->input->post('menuicon');
				$array['priority']     = $this->input->post('priority');
				$array['parentmenuID'] = $this->input->post('parentmenuID');
				$array['status']       = $this->input->post('status');
				$this->menu_m->insert_menu($array);
				$this->session->set_flashdata('message', 'Message');
				redirect('menu/index');
			}
		} else {
			$this->data["subview"] = "menu/add";
			$this->load->view('_main_layout', $this->data);
		}
	}


	public function edit() {
		$id = escapeString($this->uri->segment('3'));
		if((int)$id) {
			$this->data['menu'] = $this->menu_m->get_single_menu($id);
			if(inicompute($this->data['menu'])) {
				$this->data['parentmenus'] = $this->menu_m->get_order_by_menu(array('parentmenuID'=>0),array('menuID','menuname'));
				if($_POST) {
					$rules = $this->rules();
					$this->form_validation->set_rules($rules);
					if ($this->form_validation->run() == FALSE) {
						$this->data["subview"] = "menu/edit";
						$this->load->view('_main_layout', $this->data);
					} else {
						$array = []; 
						$array['menuname']     = $this->input->post('menuname');
						$array['menulink']     = $this->input->post('menulink');
						$array['menuicon']     = $this->input->post('menuicon');
						$array['priority']     = $this->input->post('priority');
						$array['parentmenuID'] = $this->input->post('parentmenuID');
						$array['status']       = $this->input->post('status');
						$this->menu_m->update_menu($array,$id);
						$this->session->set_flashdata('message', 'Message');
						redirect('menu/index');
					}
				} else {
					$this->data["subview"] = "menu/edit";
					$this->load->view('_main_layout', $this->data);
				}
			} else {
				$this->data["subview"] = "_not_found";
				$this->load->view('_main_layout', $this->data);
			}
		} else {
			$this->data["subview"] = "_not_found";
			$this->load->view('_main_layout', $this->data);
		}
	}


	public function delete() {
		$id = escapeString($this->uri->segment('3'));
		if((int)$id) {
			$this->menu_m->delete_menu($id);
			$this->session->set_flashdata('message', 'Message');
			redirect('menu/index');
		} else {
			$this->data["subview"] = "_not_found";
			$this->load->view('_main_layout', $this->data);
		}
	}

}
