<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Categories extends Admin_Controller{

	function __construct() {
		parent::__construct();
		$this->load->model('categories_m');
		$lang = settings()->language;
		$this->lang->load('categories', $lang);
	}

	public function index() {
		$this->data['headerassets'] = array(
			'css' => array(
				'assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css',
				'assets/custom/css/hidetable.css',
				),
			'js' => array(
				'assets/bower_components/datatables.net/js/jquery.dataTables.min.js',
				'assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js',
				'assets/pages_js/categories.js',
				)
			);
		$this->data['categories'] = $this->categories_m->get_categories();
		$this->data['title'] = 'Categories';
		$this->data["subview"] = "categories/index";
		$this->load->view('_main_layout', $this->data);
	}

	protected function rules() {
		$rules = array(
			array(
				'field'=> 'categories_name',
				'label'=> $this->lang->line('categories_name'),
				'rules'=> 'trim|required|max_length[25]',
				),
			array(
				'field'=> 'categories_note',
				'label'=> $this->lang->line('categories_note'),
				'rules'=> 'trim|max_length[60]',
				)
			);
		return $rules;
	}

	public function add(){
		if($_POST) {
			$this->form_validation->set_rules($this->rules());
			if($this->form_validation->run() == FALSE) {
				$validation_errors = $this->form_validation->verror_array();
				$json = array("confirmation" => 'error', 'validations' => $validation_errors);
				header("Content-Type: application/json", true);
				echo json_encode($json);
				exit;
			} else {

				$array = array(
					'categories_code'    => $this->input->post('categories_code'),
					'categories_name'    => $this->input->post('categories_name'),
					'categories_note'  	=> $this->input->post('categories_note'),
					'categories_status'  => '1',
					'create_date'      	=> date('Y-m-d H:i:s'),
					'create_userID'    	=> $this->session->userdata('userID'),
					'create_usertypeID'	=> $this->session->userdata('usertypeID'),
					'modify_date'      	=> date('Y-m-d H:i:s'),
					'modify_userID'    	=> $this->session->userdata('userID'),
					'modify_usertypeID'	=> $this->session->userdata('usertypeID'),
					);
				$this->categories_m->insert_categories($array);
				$json = array("confirmation" => 'Success');
				header("Content-Type: application/json", true);
				echo json_encode($json);
			}
		} else {
			redirect(base_url('categories'));
		}
	}


	public function edit(){
		if($_POST) {
			$this->form_validation->set_rules($this->rules());
			if($this->form_validation->run() == FALSE) {
				$validation_errors = $this->form_validation->verror_array();
				$json = array("confirmation" => 'error', 'validations' => $validation_errors);
				header("Content-Type: application/json", true);
				echo json_encode($json);
				exit;
			} else {
				$array = array(
					'categories_name'    => $this->input->post('categories_name'),
					'categories_note'  	=> $this->input->post('categories_note'),
					'modify_date'      	=> date('Y-m-d H:i:s'),
					'modify_userID'    	=> $this->session->userdata('userID'),
					'modify_usertypeID'	=> $this->session->userdata('usertypeID'),
					);
				$this->categories_m->update_categories($array, $this->input->post('id'));
				$json = array("confirmation" => 'Success');
				header("Content-Type: application/json", true);
				echo json_encode($json);
			} 
		}else {
			redirect(base_url('expenses'));
		}
	}

	public function retrive() {
		$categoriesID = $this->input->post('id');
		if((int)$categoriesID){
			$get_data = $this->categories_m->get_single_categories(array('categoriesID' => $categoriesID));
			if(inicompute($get_data)){
				$json = array(
					"confirmation" => 'Success',
					'id' => $get_data->categoriesID, 
					'categories_name' => $get_data->categories_name,
					'categories_note' => $get_data->categories_note,
					);
				header("Content-Type: application/json", true);
				echo json_encode($json);
				exit;
			} else {
				$json = array("confirmation" => 'error');
				header("Content-Type: application/json", true);
				echo json_encode($json);
			}
		}else {
			redirect(base_url('categories'));
		}
	}

	public function status(){
		$array = array(
			'categories_status'    => $this->input->post('status'),
			);
		$this->categories_m->update_categories($array, $this->input->post('id'));
		$json = array("confirmation" => 'Success');
		header("Content-Type: application/json", true);
		echo json_encode($json);
		
	}

	public function delete() {
		if($this->categories_m->delete_categories($this->input->post('id'))){
			$response['status']  = 'success';
			$response['message'] = 'Deleted Successfully ...';
			echo json_encode($response);
		}
	}
}
