<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Settings extends Admin_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('settings_m');
		$lang = settings()->language;
		$this->lang->load('settings', $lang);
	}

	public function index() {
		$this->data['headerassets'] = array(
			'css' => array(
				'assets/bower_components/select2/select2.css',
				),
			'js' => array(
				'assets/bower_components/select2/select2.js',
				)
			);
		if($_POST) {
			$rules = $this->rules();
			$this->form_validation->set_rules($rules);
			if ($this->form_validation->run() == FALSE) {
				$this->data["subview"] = "settings/index";
				$this->load->view('_main_layout', $this->data);
			} else {
				$array = array();
				for($i=0; $i<inicompute($rules); $i++) {
					if($this->input->post($rules[$i]['field']) == false) {
						$array[$rules[$i]['field']] = '';
					} else {
						$array[$rules[$i]['field']] = $this->input->post($rules[$i]['field']);
					}
				}
				$array['logo'] = $this->upload_data['file']['file_name'];
				$array["modify_date"] = date('Y-m-d g:i A');
				$this->settings_m->insert_or_update($array);
				$this->session->set_flashdata('message','Success');
				redirect('settings/index');
			}
		} else {
			$this->data['title'] = 'Company Profile';
			$this->data["subview"] = "settings/index";
			$this->load->view('_main_layout', $this->data);
		}
		
	}

	protected function rules() {
		$rules = array(
			array(
				'field'=> 'company_name',
				'label'=> $this->lang->line('company_name'),
				'rules'=> 'trim|required|max_length[60]',
				),
			array(
				'field'=> 'tag_line',
				'label'=> $this->lang->line('tag_line'),
				'rules'=> 'trim',
				),
			array(
				'field'=> 'type',
				'label'=> $this->lang->line('type'),
				'rules'=> 'trim',
				),
			array(
				'field'=> 'owner',
				'label'=> $this->lang->line('owner'),
				'rules'=> 'trim|required|max_length[30]',
				),
			array(
				'field'=> 'mobile_no',
				'label'=> $this->lang->line('mobile_no'),
				'rules'=> 'trim|required|is_numeric|max_length[20]',
				),
			array(
				'field'=> 'phone',
				'label'=> $this->lang->line('phone'),
				'rules'=> 'trim|is_numeric|max_length[15]',
				),
			array(
				'field'=> 'fax_no',
				'label'=> $this->lang->line('fax_no'),
				'rules'=> 'trim|is_numeric|max_length[20]',
				),
			array(
				'field'=> 'email',
				'label'=> $this->lang->line('email'),
				'rules'=> 'trim|required|valid_email',
				),

			array(
				'field'=> 'tax_no',
				'label'=> $this->lang->line('settings_tax'),
				'rules'=> 'trim|numeric|max_length[32]',
				),
			array(
				'field'=> 'language',
				'label'=> $this->lang->line('settings_language'),
				'rules'=> 'trim',
				),
			array(
				'field'=> 'address',
				'label'=> $this->lang->line('address'),
				'rules'=> 'trim|required|max_length[70]',
				),
			array(
				'field'=> 'time_zone',
				'label'=> $this->lang->line('time_zone'),
				'rules'=> 'trim|required|required_no_zero',
				),
			array(
				'field'=> 'logo',
				'label'=> $this->lang->line('logo'),
				'rules'=> 'trim|max_length[200]|callback_photo_upload',
				),
			array(
				'field'=> 'category_prefixe',
				'label'=> $this->lang->line('category_prefixe'),
				'rules'=> 'trim|required|max_length[2]|strtoupper',
				),
			array(
				'field'=> 'member_prefixe',
				'label'=> $this->lang->line('member_prefixe'),
				'rules'=> 'trim|required|max_length[2]|strtoupper',
				),
			array(
				'field'=> 'book_prefixe',
				'label'=> $this->lang->line('book_prefixe'),
				'rules'=> 'trim|required|max_length[2]|strtoupper',
				),
			array(
				'field'=> 'membership_prefixe',
				'label'=> $this->lang->line('membership_prefixe'),
				'rules'=> 'trim|required|max_length[2]|strtoupper',
				),
			array(
				'field'=> 'currency_code',
				'label'=> $this->lang->line('currency_code'),
				'rules'=> 'trim|required|max_length[5]|strtoupper',
				),
			array(
				'field'=> 'currency_symbol',
				'label'=> $this->lang->line('currency_symbol'),
				'rules'=> 'trim|required|max_length[2]',
				),
			);
return $rules;
}

public function photo_upload() {
	$settings = $this->settings_m->get_settings_data(1);
	$new_file = "default.png";
	if($_FILES["logo"]['name'] !="") {
		$file_name = $_FILES["logo"]['name'];
		$makeRandom = hash('sha512', config_item("encryption_key"));
		$file_name_rename = $makeRandom;
		$explode = explode('.', $file_name);
		if(inicompute($explode) >= 2) {
			$new_file = $file_name_rename.'.'.end($explode);
			$config['upload_path'] = "./uploads/images";
			$config['allowed_types'] = "gif|jpg|png";
			$config['file_name'] = $new_file;
			$config['max_size'] = '1024';
			$config['max_width'] = '3000';
			$config['max_height'] = '3000';
			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			if(!$this->upload->do_upload("logo")) {
				$this->form_validation->set_message("photoupload", $this->upload->display_errors());
				return FALSE;
			} else {
				$this->upload_data['file'] =  $this->upload->data();
				return TRUE;
			}
		} else {
			$this->form_validation->set_message("photoupload", "Invalid file");
			return FALSE;
		}
	} else {
		if(!inicompute($settings)){
			$this->upload_data['file'] = array('file_name' => $new_file);
			return TRUE;
		} else {
			$this->upload_data['file'] = array('file_name' => $settings->logo);
			return TRUE;
		}
	}
}

}
