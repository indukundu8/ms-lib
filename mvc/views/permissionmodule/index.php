<!-- Content Wrapper Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
		<h1>
			Permission Module
		</h1>
		<ol class="breadcrumb">
      <li><a href="<?=base_url('/')?>"><i class="fa fa-dashboard"></i> Home</a></li>
			<li><a href="<?=base_url('/permissionmodule/index')?>"><i class="fa fa-dashboard"></i> Permission Module</a></li>
		</ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="box">
            <div class="box-header">
              <a href="<?=base_url('permissionmodule/add')?>" class="btn btn-inline btn-primary btn-md"><i class="fa fa-plus"></i> Add Permission Module</a>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div id="hide-table">
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>Name</th>
                      <th>Description</th>
                      <th>Status</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php if(count($permissionmodules)) { $i=0; foreach($permissionmodules as $permissionmodule) { $i++; ?>
                    <tr>
                      <td data-title="#"><?=$i?></td>
                      <td data-title="Name"><?=$permissionmodule->name?></td>
                      <td data-title="Description"><?=$permissionmodule->description?></td>
                      <td data-title="Status"><?=$permissionmodule->active?></td>
                      <td data-title="Action">
                        <?=btn_edit_show('permissionmodule/edit/'.$permissionmodule->permissionmoduleID,'Edit')?>
                        <?=btn_delete_show('permissionmodule/delete/'.$permissionmodule->permissionmoduleID,'Delete')?>
                      </td>
                    </tr>
                    <?php } } ?>
                  </tbody>
                  <tfoot>
                    <tr>
                      <th>#</th>
                      <th>Name</th>
                      <th>Description</th>
                      <th>Status</th>
                      <th>Action</th>
                    </tr>
                  </tfoot>
                </table>
              </div>
            </div>
            <!-- /.box-body -->
          </div>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<script>
  $(function () {
    $('#example1').DataTable({
      'pageLength':25,
    });
  })
</script>