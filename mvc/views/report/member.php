<style type="text/css">
.img-height{
  height: 100px;
  width: 100px;
  float: left;
  margin-right: 10px;
}
#print_areas h3{line-height: 3px !important;}
.btn-primary{background-color: #19446a}
.btn-primary:hover{background-color: #1d4d77}
</style>
<div class="content-wrapper">
  <section class="content-header">
    <h1><?=$this->lang->line('reports_member')?></h1>
    <ol class="breadcrumb">
      <li><a href="<?=base_url('/')?>"><i class="fa fa-laptop"></i></a></li>
      <li><a href="<?=base_url('/report')?>"><?=$this->lang->line('reports_reports')?></a></li>
      <li><?=$this->lang->line('reports_member')?></li>
    </ol>
  </section>


  <section class="content">
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title"><?=$this->lang->line('report_generated')?></h3>
      </div>
      <form method="post">
        <div class="box-body">
          <div class="row">
            <input type="hidden" name="<?=$this->security->get_csrf_token_name()?>" value="<?=$this->security->get_csrf_hash()?>" />
            <div class="col-md-4">
              <div class="form-group <?=form_error('membership') ? 'has-error' : ''?>">
                <label for="reports_membership_type"><?=$this->lang->line('reports_membership_type')?></label><span class="text-red">*</span>
                <?php 
                $membershipArray[0] = $this->lang->line('reports_please_select');
                if(count($memberships)){
                  foreach ($memberships as $membership){
                    $membershipArray[$membership->membershipID] = ucwords($membership->membership_name);
                  }
                }
                echo form_dropdown('membership', $membershipArray,set_value('membership'),'id="membership" class="form-control select2"');
                ?>  
                <?=form_error('membership','<div class="text-red">', '</div>')?>           
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label for="report_status"><?=$this->lang->line('report_status')?></label>
                <?php
                $memberStatusArray[1]  = $this->lang->line('reports_member_Active');
                $memberStatusArray[0]  = $this->lang->line('reports_member_Inactive');
                echo form_dropdown('member_status', $memberStatusArray,set_value('member_status'),'id="member_status" class="form-control"');
                ?>            
              </div>
          </div>
          <div class="col-md-4">
            <label for="submit">&nbsp;</label>
            <button type="submit" class="btn btn-primary btn-flat btn-block"><?=$this->lang->line('report_generate')?></button>
          </div>
        </div><br>
      </div>
    </form>
  </div>


  <div class="box box-solid">
    <div class="box-header header-custom">
      <h5 class="box-title"><i class="fa fa-list"></i> <?=$this->lang->line('reports_reports')?></h5>
      <div class="box-tools pull-right">
        <button class="btn btn-inline btn-custom btn-md prints"><i class="fa fa-print"></i> <?=$this->lang->line('report_print')?></button>
      </div>
    </div>
    <div class="box-body" id='print_areas'>
      <div>
        <div class="row">
          <div class=" col-xs-4">
            <img class="img-responsive img-height" src="<?=base_url('uploads/images/'.settings()->logo)?>">
            <div>
              <h4 class="text-capitalize"><?=settings()->company_name?></h4>
              <span><?=settings()->address?></span><br>
              <span>Phone: <?=settings()->phone?></span><br>
              <span>Email: <?=settings()->email?></span>
            </div>
          </div>
          <div class=" col-xs-8 text-center">
            <h3><?=$this->lang->line('reports_member')?></h3>
            <span><b><?=$this->lang->line('member_type')?>:&nbsp;</b><?=ucwords(!empty($membershipname)?$membershipname->membership_name:'&nbsp;')?></span><br>
            <span><b><?=$this->lang->line('report_status')?>:&nbsp;</b><?=!empty($status)?$status :'&nbsp;'?></span><br>
            <span><?=date('d-M-Y')?></span>
          </div>
        </div>
      </div><br>
      <div id="hide-table">
                  <table id="example1" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>#</th>
                <th><?=$this->lang->line('report_photo')?></th>
                <th><?=$this->lang->line('report_code')?></th>
                <th><?=$this->lang->line('member_name')?></th>
                <th><?=$this->lang->line('member_parents')?></th>
                <th><?=$this->lang->line('member_address')?></th>
                <th><?=$this->lang->line('member_phone')?></th>
                <th><?=$this->lang->line('member_occupation')?></th>
                <th><?=$this->lang->line('member_nid')?></th>
                <th><?=$this->lang->line('report_remark')?></th>
              </tr>
            </thead>
            <tbody >
              <?php if(count($result)) { $i=0; foreach($result as $val) { $i++; ?>
              <tr>
                <td data-title="#"><?=$i?></td>
                <td data-title="<?=$this->lang->line('report_photo')?>"><img src="<?=member_img($val->member_photo)?>" class="profile_img" alt="<?=$val->member_name?>"></td>
                <td data-title="<?=$this->lang->line('report_code')?>"><?=ucwords($val->member_code)?></td>
                <td data-title="<?=$this->lang->line('member_name')?>"><?=ucwords($val->member_name)?></td>
                <td data-title="<?=$this->lang->line('member_parents')?>"><?=ucwords($val->member_father_name)?><br><?=ucwords($val->member_mother_name)?></td>
                <td data-title="<?=$this->lang->line('member_address')?>"><?=ucwords($val->member_address)?></td>
                <td data-title="<?=$this->lang->line('member_phone')?>"><?=ucwords($val->member_phone)?></td>
                <td data-title="<?=$this->lang->line('member_occupation')?>"><?=ucwords($val->member_occupation)?></td>
                <td data-title="<?=$this->lang->line('member_nid')?>"><?=ucwords(($val->member_nid_no)?$val->member_nid_no:'&nbsp;')?> </td>
                <td data-title="<?=$this->lang->line('report_remark')?>">&nbsp;</td>
              </tr>
              <?php } } ?>
            </tbody>
          </table>
      </div>

    </div>
  </div>
</section>
</div>