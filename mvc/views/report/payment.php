<style type="text/css">
.img-height{
  height: 100px;
  width: 100px;
  float: left;
  margin-right: 10px;
}
#print_areas h3{line-height: 3px !important;}
.btn-primary{background-color: #19446a}
.btn-primary:hover{background-color: #1d4d77}
</style>
<div class="content-wrapper">
  <section class="content-header">
    <h1><?=$this->lang->line('report_payment')?></h1>
    <ol class="breadcrumb">
      <li><a href="<?=base_url('/')?>"><i class="fa fa-laptop"></i></a></li>
      <li><a href="<?=base_url('/report')?>"><?=$this->lang->line('reports_reports')?></a></li>
      <li><?=$this->lang->line('report_payment')?></li>
    </ol>
  </section>


  <section class="content">
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title"><?=$this->lang->line('report_generated')?></h3>
      </div>
      <form method="post">
        <div class="box-body">
          <div class="row">
            <input type="hidden" name="<?=$this->security->get_csrf_token_name()?>" value="<?=$this->security->get_csrf_hash()?>" />

              <div class="col-md-3">
                <div class="form-group">
                  <label for="tag_line"><?=$this->lang->line('reports_from_date')?></label>
                  <input type="text" class="form-control datepicker" id="from_date" name="from_date" value="<?=date('d-m-Y')?>" readonly>
                </div>
              </div>
              <div class="col-md-3">
               <div class="form-group">
                <label for="type"><?=$this->lang->line('reports_to_date')?></label>
                <input type="text" class="form-control datepicker" id="to_date" name="to_date" value="<?=date('d-m-Y')?>" readonly>
              </div>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                <label for="reports_for"><?=$this->lang->line('reports_for')?></label>
                <?php 
                  $membershipArray[0] = $this->lang->line('reports_please_select');
                  $membershipArray['lost_book']   = $this->lang->line('reports_lost_book');
                  $membershipArray['expiry_date'] = $this->lang->line('reports_expiry_date');
                  $membershipArray['membership']  = $this->lang->line('reports_membership_fee');
                  echo form_dropdown('for', $membershipArray,set_value('for'),'id="for" class="form-control select2"');
                ?>  
              </div>
            </div>
          <div class="col-md-3">
            <label for="submit">&nbsp;</label>
            <button type="submit" class="btn btn-primary btn-flat btn-block"><?=$this->lang->line('report_generate')?></button>
          </div>
        </div><br>
      </div>
    </form>
  </div>


  <div class="box box-solid">
    <div class="box-header header-custom">
      <h5 class="box-title"><i class="fa fa-list"></i> <?=$this->lang->line('reports_reports')?></h5>
      <div class="box-tools pull-right">
        <button class="btn btn-inline btn-custom btn-md prints"><i class="fa fa-print"></i> <?=$this->lang->line('report_print')?></button>
      </div>
    </div>
    <div class="box-body" id='print_areas'>
      <div>
        <div class="row">
          <div class=" col-xs-4">
            <img class="img-responsive img-height" src="<?=base_url('uploads/images/'.settings()->logo)?>">
            <div>
              <h4 class="text-capitalize"><?=settings()->company_name?></h4>
              <span><?=settings()->address?></span><br>
              <span>Phone: <?=settings()->phone?></span><br>
              <span>Email: <?=settings()->email?></span>
            </div>
          </div>
          <div class=" col-xs-8 text-center">
            <h3><?=$this->lang->line('report_payment')?></h3><br>
            <span><?=(count($date)? '<p>'.date('d-M-Y',strtotime($date["from_date"])).'<b>&nbsp;To&nbsp;</b>'.date('d-M-Y',strtotime($date["to_date"])).'</p>' : '')?></span>
          </div>
        </div>
      </div><br>
      <div id="hide-table">
        <table id="example1" class="table table-bordered table-striped">
          <thead>
            <tr>
              <th>#</th>
              <th><?=$this->lang->line('payment_date')?></th>
              <th><?=$this->lang->line('payment_member_name')?></th>
              <th><?=$this->lang->line('payment_for')?></th>
              <th><?=$this->lang->line('payment_amount')?></th>
              <th><?=$this->lang->line('report_remark')?></th>
            </tr>
          </thead>
          <tbody >
            <?php if(count($result)) { $i=0; foreach($result as $val) { $i++; ?>
            <tr>
              <td data-title="#"><?=$i?></td>
              <td data-title="<?=$this->lang->line('payment_date')?>"><?=app_date($val->create_date)?></td>
              <td data-title="<?=$this->lang->line('payment_member_name')?>"><?=(isset($membername[$val->payment_memberID])?ucwords($membername[$val->payment_memberID]): '&nbsp;');?></td>
              <td data-title="<?=$this->lang->line('payment_for')?>"><?=ucwords(str_replace('_', ' ', $val->payment_by));?></td>
              <td data-title="<?=$this->lang->line('payment_amount')?>"><?= settings()->currency_symbol.$val->payment_amount?></td>
              <td data-title="<?=$this->lang->line('report_remark')?>"></td>
            </tr>
            <?php } } ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</section>
</div>
<script type="text/javascript">
$('.datepicker').datepicker({
  autoclose: true,
  format : 'dd-mm-yyyy',
});
</script>