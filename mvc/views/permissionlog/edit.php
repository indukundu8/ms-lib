<!-- Content Wrapper Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Permissionlog
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?=base_url('/')?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?=base_url('/permissionlog')?>"><i class="fa fa-dashboard"></i> Permissionlog</a></li>
        <li><a href="<?=base_url('/permissionlog/update')?>"><i class="fa fa-dashboard"></i> Update</a></li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="box">
        <div class="row">
          <div class="col-md-6">
            <!-- /.box-header -->
            <form role="form" method="POST">
              <div class="box-body">
                <input type="hidden" name="<?=$this->security->get_csrf_token_name()?>" value="<?=$this->security->get_csrf_hash()?>" />
                <div class="form-group <?=form_error('name') ? 'has-error' : ''?>">
                  <label for="name">Name</label> <span class="text-red">*</span>
                  <input type="text" class="form-control" value="<?=set_value('name',$permissionlog->name)?>" id="name" name="name"/>
                  <?=form_error('name')?>
                </div>
                <div class="form-group <?=form_error('description') ? 'has-error' : ''?>">
                  <label for="description">Description</label> <span class="text-red">*</span>
                  <textarea name="description" id="description" cols="30" rows="3" class="form-control"><?=set_value('description',$permissionlog->description)?></textarea>
                  <?=form_error('description')?>
                </div>
                <div class="form-group <?=form_error('permissionmoduleID') ? 'has-error' : ''?>">
                  <label for="permissionmoduleID">Permission Module</label> <span class="text-red">*</span>
                  <?php 
                    $permissionmoduleArray[0] = "Please Select";
                    if(count($permissionmodules)) {
                      foreach ($permissionmodules as $permissionmodule) {
                        $permissionmoduleArray[$permissionmodule->permissionmoduleID] = $permissionmodule->name;
                      }
                    }
                    echo form_dropdown('permissionmoduleID', $permissionmoduleArray,set_value('permissionmoduleID',$permissionlog->permissionmoduleID),'id="permissionmoduleID" class="form-control"');
                  ?>
                  <?=form_error('permissionmoduleID')?>
                </div>
                <div class="form-group <?=form_error('active') ? 'has-error' : ''?>">
                  <label for="active">Active</label> <span class="text-red">*</span>
                  <input type="text" class="form-control" value="<?=set_value('active',$permissionlog->active)?>" id="active" name="active" />
                  <?=form_error('active')?>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Update Permissionlog</button>
              </div>
            </form>
            <!-- /.box-body -->
          </div>
        </div>
      </div>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->