<style type="text/css">.note-line-height{line-height: 14px;}</style>
<div class="content-wrapper">
  <section class="content-header">
    <h1><?=$this->lang->line('membership_membership')?></h1>
    <ol class="breadcrumb">
      <li><a href="<?=base_url('/')?>"><i class="fa fa-laptop"></i></a></li>
      <li><a href="<?=base_url('/membership')?>"><?=$this->lang->line('membership_membership')?></a></li>
    </ol>
  </section>
  <section class="content">
    <div class="box box-solid">
      <div class="box-header header-custom">
        <h5 class="box-title"><i class="fa fa-list"></i> <?=$this->lang->line('membership_list')?></h5>
        <div class="box-tools pull-right">
          <?=add_btn('membership_add',$this->lang->line('membership_add_membership'));?>
        </div>
      </div>
      <div class="box-body">
        <div id="hide-table">
          <table id="example1" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>#</th>
                <th><?=$this->lang->line('membership_code')?></th>
                <th><?=$this->lang->line('membership_name')?></th>
                <th><?=$this->lang->line('membership_limit_book')?></th>
                <th><?=$this->lang->line('membership_limit_day')?></th>
                <th><?=$this->lang->line('membership_fee')?></th>
                <th><?=$this->lang->line('membership_penalty_fee')?></th>
                <th><?=$this->lang->line('membership_renew_limit')?></th>
                <th><?=$this->lang->line('membership_status')?></th>
                <th><?=$this->lang->line('membership_action')?></th>
              </tr>
            </thead>
            <tbody >
              <?php if(count($memberships)) { $i=0; foreach($memberships as $val) { $i++; ?>
              <tr>
                <td data-title="#"><?=$i?></td>
                <td data-title="<?=$this->lang->line('membership_code')?>"><?=$val->membership_code?></td>
                <td data-title="<?=$this->lang->line('membership_name')?>"><?=$val->membership_name?></td>
                <td data-title="<?=$this->lang->line('membership_limit_book')?>"><?=$val->membership_books_limit?></td>
                <td data-title="<?=$this->lang->line('membership_limit_day')?>"><?=$val->membership_days_limit?></td>
                <td data-title="<?=$this->lang->line('membership_renew_limit')?>"><?=(!empty($val->membership_fee)?settings()->currency_symbol.$val->membership_fee: $this->lang->line('membership_free'))?></td>

                <td data-title="<?=$this->lang->line('membership_penalty_fee')?>"><?=(!empty($val->penalty_fee)?settings()->currency_symbol.$val->penalty_fee: $this->lang->line('membership_free'))?></td>
                <td data-title="<?=$this->lang->line('membership_renew_limit')?>"><?=$val->renew_limit?></td>

                <td data-title="<?=$this->lang->line('membership_status')?>">
                  <div class="onoffswitch-small" id="<?=$val->membershipID?>">
                    <input type="checkbox" id="myonoffswitch<?=$val->membershipID?>" class="onoffswitch-small-checkbox" name="membership_status" <?php if($val->membership_status === '1') echo "checked='checked'"; ?> <?=permissionChecker('membership_status')?'':'disabled'?>>
                    <label for="myonoffswitch<?=$val->membershipID?>" class="onoffswitch-small-label">
                      <span class="onoffswitch-small-inner"></span>
                      <span class="onoffswitch-small-switch <?=permissionChecker('membership_status')?'':'hidden'?>"></span>
                    </label>
                  </div>
                </td>
                <td data-title="<?=$this->lang->line('membership_action')?>">
                  <?=edit_btn('membership_edit',$val->membershipID); ?>&nbsp;
                  <?=delete_btn('membership_delete',$val->membershipID); ?>
                </td>
              </tr>
              <?php } } ?>
            </tbody>
          </table>
          <br>
          <div class="note-line-height">
            <p class="text-danger"><?=$this->lang->line('membership_note')?></p>
            <p class="text-muted"><?=$this->lang->line('membership_penalty_note')?></p>
            <p class="text-muted"><?=$this->lang->line('membership_renew_note')?></p>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>


<div class="modal fade" id="insert" tabindex="-1">
  <div class="modal-dialog modal-sm">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><i class="icofont-plus"></i></span>&nbsp;<?=$this->lang->line('membership_add_membership')?></h4>
      </div>
      <div class="modal-body">
        <div class="form-group error-name">
          <input type="hidden" class="form-control" name="membership_code" id="membership_code" value="<?=settings()->membership_prefixe;membershipcode();?>" readonly/>
          <label><?=$this->lang->line('membership_name')?> <span class='text-danger'>*</span></label>
          <input type="text" class="form-control" name="membership_name" id="membership_name" placeholder="<?=$this->lang->line('membership_name')?>"/>
          <span class="text-red" id="error_name"></span>
        </div>
        <div class="form-group error-limit-book">
          <label><?=$this->lang->line('membership_limit_book')?> <span class='text-danger'>*</span></label>
          <input type="text" name='membership_limit_book' id="membership_limit_book" class="form-control only_number" placeholder="<?=$this->lang->line('membership_limit_book')?>"/>
          <span class="text-red" id="error_limit_book"></span>
        </div>
        <div class="form-group error-limit-day">
          <label><?=$this->lang->line('membership_limit_day')?> <span class='text-danger'>*</span></label>
          <input type="text" name='membership_limit_day' id="membership_limit_day" class="form-control only_number" placeholder="<?=$this->lang->line('membership_limit_day')?>"/>
          <span class="text-red" id="error_limit_day"></span>
        </div>

        <div class="form-group">
           <label><?=$this->lang->line('membership_fee')?></label>
            <div class="input-group">
              <span class="input-group-addon"><?=settings()->currency_symbol?></span>
              <input type="text" name='membership_fee' id="membership_fee" class="form-control only_number" placeholder="<?=$this->lang->line('membership_fee')?>"/>
            </div>
        </div>

        <div class="form-group">
           <label><?=$this->lang->line('membership_penalty_fee')?></label>
            <div class="input-group">
              <span class="input-group-addon"><?=settings()->currency_symbol?></span>
              <input type="text" name='membership_penalty_fee' id="membership_penalty_fee" class="form-control only_number" placeholder="<?=$this->lang->line('membership_penalty_fee')?>"/>
            </div>
        </div>

        <div class="form-group error-renew-limit">
          <label><?=$this->lang->line('membership_renew_limit')?><span class='text-danger'>*</span></label>
          <input type="text" name='membership_renew_limit' id="membership_renew_limit" class="form-control only_number" placeholder="<?=$this->lang->line('membership_renew_limit')?>"/>
          <span class="text-red" id="error_renew_limit"></span>
        </div>

      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-default insert">Save</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="update" tabindex="-1">
  <div class="modal-dialog modal-sm">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><i class="fa fa-edit"></i></span>&nbsp;<?=$this->lang->line('membership_update')?></h4>
      </div>
      <div class="modal-body">
        <div class="">
          <input type="hidden" class="form-control" name="membershipID" id="membershipID" readonly/>
          <div class="form-group error-name-up">
            <label><?=$this->lang->line('membership_name')?> <span class='text-danger'>*</span></label>
            <input type="text" class="form-control" name="membership_name" id="membership_name_up" placeholder="<?=$this->lang->line('membership_name')?>"/>
            <span class="text-red" id="error_name_up"></span>
          </div>
        <div class="form-group error-limit-book-up">
          <label><?=$this->lang->line('membership_limit_book')?> <span class='text-danger'>*</span></label>
          <input type="text" name='membership_limit_book' id="membership_books_limit_up" class="form-control only_number" placeholder="<?=$this->lang->line('membership_limit_book')?>"/>
          <span class="text-red" id="error_limit_book_up"></span>
        </div>
        <div class="form-group error-limit-day-up">
          <label><?=$this->lang->line('membership_limit_day')?> <span class='text-danger'>*</span></label>
          <input type="text" name='membership_limit_day' id="membership_days_limit_up" class="form-control only_number" placeholder="<?=$this->lang->line('membership_limit_day')?>"/>
          <span class="text-red" id="error_limit_day_up"></span>
        </div>
        <div class="form-group">
          <label><?=$this->lang->line('membership_fee')?></label>
            <div class="input-group">
              <span class="input-group-addon"><?=settings()->currency_symbol?></span>
              <input type="text" name='membership_fee_up' id="membership_fee_up" class="form-control only_number" placeholder="<?=$this->lang->line('membership_fee')?>"/>
            </div>
          </div>

          <div class="form-group">
           <label><?=$this->lang->line('membership_penalty_fee')?></label>
            <div class="input-group">
              <span class="input-group-addon"><?=settings()->currency_symbol?></span>
              <input type="text" name='membership_penalty_fee_up' id="membership_penalty_fee_up" class="form-control only_number" placeholder="<?=$this->lang->line('membership_penalty_fee')?>"/>
            </div>
        </div>

        <div class="form-group error-renew-limit-up">
          <label><?=$this->lang->line('membership_renew_limit')?><span class='text-danger'>*</span></label>
          <input type="text" name='membership_renew_limit_up' id="membership_renew_limit_up" class="form-control only_number" placeholder="<?=$this->lang->line('membership_renew_limit')?>"/>
          <span class="text-red" id="error_renew_limit_up"></span>
        </div>


        </div>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-info updated"><?=$this->lang->line('membership_update')?></button>
      </div>
    </div>
  </div>
</div>
