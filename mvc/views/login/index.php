<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?=settings()->company_name?> <?=(!empty($title)?'| '.$title : '')?></title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?=base_url('assets/bower_components/bootstrap/dist/css/bootstrap.min.css')?>">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?=base_url('assets/bower_components/font-awesome/css/font-awesome.min.css')?>">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?=base_url('assets/bower_components/Ionicons/css/ionicons.min.css')?>">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?=base_url('assets/dist/css/AdminLTE.min.css')?>">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?=base_url('assets/plugins/iCheck/square/blue.css')?>">
  <style type="text/css">.glyphicon-xl{font-size: 100px;color: skyblue;} .h1-header{font-size: 36px;} .login-box-msg{padding:0px !important;}</style>

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>


<body class="hold-transition login-page zero-height">
<div class="login-box">
  <div class="login-logo">
    <a href="<?=base_url('/')?>" class="text-capitalize"><b><?=settings()->company_name;?></b></a>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg"><span class="glyphicon glyphicon-lock glyphicon-xl"></span></p>
    <p class="login-box-msg">Sign in to start your session</p>
    <?php if(count($errors)) {
      foreach($errors as $error) {
        echo "<p class='text-red'>".$error."</p>";
      }
    } ?>
    <form action="<?=base_url('login/index')?>" method="post">
      <input type="hidden" name="<?=$this->security->get_csrf_token_name()?>" value="<?=$this->security->get_csrf_hash()?>" />
      <div class="form-group has-feedback">
        <label><?=$this->lang->line('login_username')?></label> <span class="text-red">*</span>
        <input type="text" class="form-control" name="username" value="<?=set_value('username')?>"/>
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <label><?=$this->lang->line('login_password')?></label> <span class="text-red">*</span>
        <input type="password" class="form-control" name="password" value="<?=set_value('password')?>">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="row">

        <div class="col-xs-8">
          <div class="checkbox">
            <label>
              <input class="icheck" type="checkbox"> Remember Me
            </label><br>
            <a href="<?= base_url('login/forgot')?>"><?=$this->lang->line('forgot_password')?></a>
          </div>
        </div>
        <!-- /.col -->
        <div class="col-xs-4">
          <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
        </div>
      </div>
    </form>
  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<!-- jQuery 3 -->
<script src="<?=base_url('assets/bower_components/jquery/dist/jquery.min.js')?>"></script>
<!-- Bootstrap 3.3.7 -->
</body>
</html>
