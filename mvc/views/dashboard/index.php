<!-- Content Wrapper Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1><?=$this->lang->line('dashboard_dashboard')?></h1>
    <ol class="breadcrumb">
     <li><a href="<?=base_url('/')?>"><i class="fa fa-laptop"></i></a></li>
     <li class="active"><?=$this->lang->line('dashboard_dashboard')?></li>
   </ol>
 </section>
<br>
 <!-- Main content -->
 <section class="content">
  <div class="row">
    <div class="col-lg-3 col-xs-6">
      <div class="small-box bg-blue">
        <div class="inner">
          <h3><?php $total_sum = 0; foreach ($member as $value){  $total_sum += inicompute($value->memberID);} echo $total_sum;?></h3>
          <p><?=$this->lang->line('dashboard_member')?></p>
        </div>
        <div class="icon">
          <i class="ion ion-ios-people"></i>
        </div>
        <a href="<?=base_url('member')?>" class="small-box-footer"><?=$this->lang->line('dashboard_more')?> <i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div>
    <div class="col-lg-3 col-xs-6">
      <div class="small-box bg-orange">
        <div class="inner">
          <h3><?php $total_sum = 0; foreach ($book as $value){  $total_sum += $value->book_quantity;} echo $total_sum;?></h3>
          <p><?=$this->lang->line('dashboard_book')?></p>
        </div>
        <div class="icon">
          <i class="ion ion-ios-book"></i>
        </div>
        <a href="<?=base_url('book')?>" class="small-box-footer"><?=$this->lang->line('dashboard_more')?> <i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div>
    <div class="col-lg-3 col-xs-6">
      <div class="small-box  bg-purple">
        <div class="inner">
          <h3><?php $total_sum = 0; foreach ($categories as $value){  $total_sum += inicompute($value->categoriesID);} echo $total_sum;?></h3>
          <p><?=$this->lang->line('dashboard_categories')?></p>
        </div>
        <div class="icon">
          <i class="ion ion-ios-pricetags"></i>
        </div>
        <a href="<?=base_url('categories')?>" class="small-box-footer"><?=$this->lang->line('dashboard_more')?> <i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div>
    <div class="col-lg-3 col-xs-6">
      <div class="small-box bg-olive">
        <div class="inner">
          <h3><?php $total_sum = 0; foreach ($membership as $value){  $total_sum += inicompute($value->membershipID);} echo $total_sum;?></h3>
          <p><?=$this->lang->line('dashboard_membership')?></p>
        </div>
        <div class="icon">
          <i class="ion ion-stats-bars"></i>
        </div>
        <a href="<?=base_url('membership')?>" class="small-box-footer"><?=$this->lang->line('dashboard_more')?> <i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-lg-3 col-xs-6">
      <div class="small-box bg-green">
        <div class="inner">
          <h3><?php $total_sum = 0; foreach ($circulation as $value){  $total_sum += inicompute($value->circulationID);} echo $total_sum;?></h3>
          <p><?=$this->lang->line('dashboard_issued')?></p>
        </div>
        <div class="icon">
          <i class="ion ion-bag"></i>
        </div>
        <a href="<?=base_url('circulation')?>" class="small-box-footer"><?=$this->lang->line('dashboard_more')?> <i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div>

    <div class="col-lg-3 col-xs-6">
      <div class="small-box bg-red">
        <div class="inner">
          <h3><?php $total_sum = 0; foreach ($wastage as $value){  $total_sum += $value->wastage_quantity;} echo $total_sum;?></h3>
          <p><?=$this->lang->line('dashboard_wastage')?></p>
        </div>
        <div class="icon">
          <i class="ion ion-trash-b"></i>
        </div>
        <a href="<?=base_url('wastage')?>" class="small-box-footer"><?=$this->lang->line('dashboard_more')?> <i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div>

    <div class="col-lg-3 col-xs-6">
      <div class="small-box bg-aqua">
        <div class="inner">
          <h3><?php $total_sum = 0; foreach ($publication as $value){  $total_sum += inicompute($value->publicationID);} echo $total_sum;?></h3>
          <p><?=$this->lang->line('dashboard_publication')?></p>
        </div>
        <div class="icon">
          <i class="ion ion-ios-pricetags"></i>
        </div>
        <a href="<?=base_url('publication')?>" class="small-box-footer"><?=$this->lang->line('dashboard_more')?> <i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div>

    <div class="col-lg-3 col-xs-6">
      <div class="small-box bg-maroon">
        <div class="inner">
          <h3><?php $total_sum = 0; foreach ($writer as $value){  $total_sum += inicompute($value->writerID);} echo $total_sum;?></h3>
          <p><?=$this->lang->line('dashboard_writer')?></p>
        </div>
        <div class="icon">
          <i class="ion ion-pie-graph"></i>
        </div>
        <a href="<?=base_url('writer')?>" class="small-box-footer"><?=$this->lang->line('dashboard_more')?> <i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div>
  </div>
  <div class="row">
    <section class="col-lg-7 connectedSortable">
      <div class="box box-solid box-default">
        <div class="box-header header-custom">
          <h4 class="box-title"><?=$this->lang->line('dashboard_bar_chat')?></h4>
        </div>
        <div class="box-body">
          <div class="chart">
            <canvas id="barChart" style="height:303px"></canvas>
          </div>
        </div>
      </div>
    </section>
    <section class="col-lg-5 connectedSortable">
      <div class="box box-solid box-default">
        <div class="box-header header-custom">
          <h4 class="box-title"><?=$this->lang->line('dashboard_categories')?></h4>
        </div>
        <div class="box-body">
          <div id="hide-table">
            <table id="example1" class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th>#</th>
                  <th><?=$this->lang->line('categories_code')?></th>
                  <th><?=$this->lang->line('categories_name')?></th>
                </tr>
              </thead>
              <tbody >
                <?php if(count($categories)) { $i=0; foreach($categories as $category) { $i++; ?>
                <tr>
                  <td data-title="#"><?=$i?></td>
                  <td data-title="<?=$this->lang->line('categories_code')?>"><?=ucfirst($category->categories_code)?></td>
                  <td data-title="<?=$this->lang->line('categories_name')?>"><?=ucfirst($category->categories_name)?></td>
                </tr>
                <?php } } ?>
              </tbody>
            </table>
            </div>
          </div>
        </div>
      </section>
    </div>
    <div class="row">
      <section class="col-lg-6 connectedSortable">
        <div class="box box-solid box-default">
          <div class="box-header header-custom">
            <h4 class="box-title"><?=$this->lang->line('dashboard_issued_book')?></h4>
          </div>
          <div class="box-body">
            <div id="hide-table">
                <table class="table table-bordered table-striped">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th><?=$this->lang->line('circulation_member_name')?></th>
                      <th><?=$this->lang->line('circulation_book_name')?></th>
                      <th><?=$this->lang->line('circulation_writer_name')?></th>
                      <th><?=$this->lang->line('circulation_issue_date')?></th>
                      <th><?=$this->lang->line('circulation_expiry_date')?></th>                     
                    </tr>
                  </thead>
                  <tbody >
                    <?php if(inicompute($circulation_issue)) { $i=0; foreach($circulation_issue as $val) { $i++; ?>
                    <tr>
                      <td data-title="#"><?=$i?></td>
                      <td data-title="<?=$this->lang->line('circulation_member_name')?>"><?=(isset($membername[$val->memberID])?ucwords($membername[$val->memberID]): '&nbsp;');?></td>
                      <td data-title="<?=$this->lang->line('circulation_book_name')?>"><?=(isset($bookname[$val->bookID])?ucwords($bookname[$val->bookID]): '&nbsp;');?></td>
                      <td data-title="<?=$this->lang->line('circulation_writer_name')?>"><?=(isset($writerID[$val->bookID])?ucwords($writer_name[$writerID[$val->bookID]]): '&nbsp;');?></td>
                      <td data-title="<?=$this->lang->line('circulation_issue_date')?>"><?=(!empty($val->issue_date)?app_date($val->issue_date):'&nbsp;')?></td>
                      <td data-title="<?=$this->lang->line('circulation_expiry_date')?>"><?=app_date($val->expiry_date)?></td>
                    </tr>
                    <?php } } ?>
                  </tbody>
                </table>
              </div>
            </div>
            <div class='box-footer text-center'>
              <a href="<?=base_url('circulation')?>" class="small-box-footer"><?=$this->lang->line('dashboard_more')?> <i class="fa fa-arrow-circle-right"></i></a>
            </div>
          </div>
        </section>
        <section class="col-lg-6 connectedSortable">
          <div class="box box-solid box-default">
            <div class="box-header header-custom">
              <h4 class="box-title"><?=$this->lang->line('dashboard_returned_book')?></h4>
            </div>
            <div class="box-body">
              <div id="hide-table">
                <table class="table table-bordered table-striped">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th><?=$this->lang->line('circulation_member_name')?></th>
                      <th><?=$this->lang->line('circulation_book_name')?></th>
                      <th><?=$this->lang->line('circulation_writer_name')?></th>
                      <th><?=$this->lang->line('circulation_issue_date')?></th>
                      <th><?=$this->lang->line('circulation_expiry_date')?></th> 
                      <th><?=$this->lang->line('circulation_return_date')?></th>                    
                    </tr>
                  </thead>
                  <tbody >
                    <?php if(inicompute($circulation_return)) { $i=0; foreach($circulation_return as $val) { $i++; ?>
                    <tr>
                      <td data-title="#"><?=$i?></td>
                      <td data-title="<?=$this->lang->line('circulation_member_name')?>"><?=(isset($membername[$val->memberID])?ucwords($membername[$val->memberID]): '&nbsp;');?></td>
                      <td data-title="<?=$this->lang->line('circulation_book_name')?>"><?=(isset($bookname[$val->bookID])?ucwords($bookname[$val->bookID]): '&nbsp;');?></td>
                      <td data-title="<?=$this->lang->line('circulation_writer_name')?>"><?=(isset($writerID[$val->bookID])?ucwords($writer_name[$writerID[$val->bookID]]): '&nbsp;');?></td>
                      <td data-title="<?=$this->lang->line('circulation_issue_date')?>"><?=(!empty($val->issue_date)?app_date($val->issue_date):'&nbsp;')?></td>
                      <td data-title="<?=$this->lang->line('circulation_expiry_date')?>"><?=app_date($val->expiry_date)?></td>
                      <td data-title="<?=$this->lang->line('circulation_return_date')?>"><?=app_date($val->return_date)?></td>
                    </tr>
                    <?php } } ?>
                  </tbody>
                </table>
              </div>
            </div>
            <div class='box-footer text-center'>
              <a href="<?=base_url('circulation')?>" class="small-box-footer"><?=$this->lang->line('dashboard_more')?> <i class="fa fa-arrow-circle-right"></i></a>
            </div>
          </div>
        </section>
      </div>
    </section>
  </div>
    <script>
    $(function () {
      $('#example1').DataTable({
        'pageLength':5,
        'searching': false,
        // 'lengthMenu': false,
        dom: 'Bfrtip',

      });
    });

   

    $(function () {
      var areaChartData = {
        labels  : ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul','Aug','Sep','Oct','Nov','Dec'],
        datasets: [
        {
          label               : 'Purchases',
          fillColor           : 'rgba(210, 214, 222, 1)',
          strokeColor         : 'rgba(210, 214, 222, 1)',
          pointColor          : 'rgba(210, 214, 222, 1)',
          pointStrokeColor    : '#c1c7d1',
          pointHighlightFill  : '#fff',
          pointHighlightStroke: 'rgba(220,220,220,1)',
          data                : [<?=$yearlyissued?>]
        },
        {
          label               : 'Sales',
          fillColor           : 'rgba(60,141,188,0.9)',
          strokeColor         : 'rgba(60,141,188,0.8)',
          pointColor          : '#3b8bba',
          pointStrokeColor    : 'rgba(60,141,188,1)',
          pointHighlightFill  : '#fff',
          pointHighlightStroke: 'rgba(60,141,188,1)',
          data                : [<?=$yearlyreturned?>]

        }
        ]
      }

    //-------------
    //- BAR CHART -
    //-------------
    var barChartCanvas                   = $('#barChart').get(0).getContext('2d')
    var barChart                         = new Chart(barChartCanvas)
    var barChartData                     = areaChartData
    barChartData.datasets[1].fillColor   = '#00a65a'
    barChartData.datasets[1].strokeColor = '#00a65a'
    barChartData.datasets[1].pointColor  = '#00a65a'
    var barChartOptions                  = {
      //Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
      scaleBeginAtZero        : true,
      //Boolean - Whether grid lines are shown across the chart
      scaleShowGridLines      : true,
      //String - Colour of the grid lines
      scaleGridLineColor      : 'rgba(0,0,0,.05)',
      //Number - Width of the grid lines
      scaleGridLineWidth      : 1,
      //Boolean - Whether to show horizontal lines (except X axis)
      scaleShowHorizontalLines: true,
      //Boolean - Whether to show vertical lines (except Y axis)
      scaleShowVerticalLines  : true,
      //Boolean - If there is a stroke on each bar
      barShowStroke           : true,
      //Number - Pixel width of the bar stroke
      barStrokeWidth          : 2,
      //Number - Spacing between each of the X value sets
      barValueSpacing         : 5,
      //Number - Spacing between data sets within X values
      barDatasetSpacing       : 1,
      //String - A legend template
      legendTemplate          : '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<datasets.length; i++){%><li><span style="background-color:<%=datasets[i].fillColor%>"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>',
      //Boolean - whether to make the chart responsive
      responsive              : true,
      maintainAspectRatio     : true
    }

    barChartOptions.datasetFill = false
    barChart.Bar(barChartData, barChartOptions)
  })
</script>