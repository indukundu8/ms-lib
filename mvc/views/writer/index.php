<style type="text/css">textarea{resize: none}</style>
<div class="content-wrapper">
  <section class="content-header">
    <h1><?=$this->lang->line('writer_writer')?></h1>
    <ol class="breadcrumb">
      <li><a href="<?=base_url('/')?>"><i class="fa fa-laptop"></i></a></li>
      <li><a href="<?=base_url('/writer')?>"><?=$this->lang->line('writer_writer')?></a></li>
    </ol>
  </section>

  <section class="content">
    <div class="box box-solid">
      <div class="box-header header-custom">
        <h5 class="box-title"><i class="fa fa-list"></i> <?=$this->lang->line('writer_list')?></h5>
        <div class="box-tools pull-right">
          <?=add_btn('writer_add', $this->lang->line('writer_add_writer'));?>
        </div>
      </div>
      <div class="box-body">
        <div id="hide-table">
          <table id="example1" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>#</th>
                <th><?=$this->lang->line('writer_name')?></th>
                <th><?=$this->lang->line('writer_note')?></th>
                <th><?=$this->lang->line('writer_status')?></th>
                <th><?=$this->lang->line('writer_action')?></th>
              </tr>
            </thead>
            <tbody >
              <?php if(count($writer)) { $i=0; foreach($writer as $val) { $i++; ?>
              <tr>
                <td data-title="#"><?=$i?></td>
                <td data-title="<?=$this->lang->line('writer_name')?>"><?=ucfirst($val->writer_name)?></td>
                <td data-title="<?=$this->lang->line('writer_note')?>"><?=(!empty($val->writer_note)?$val->writer_note : '&nbsp;')?></td>
                <td data-title="<?=$this->lang->line('writer_status')?>">
                  <div class="onoffswitch-small" id="<?=$val->writerID?>">
                    <input type="checkbox" id="myonoffswitch<?=$val->writerID?>" class="onoffswitch-small-checkbox" name="writer_status" <?php if($val->writer_status === '1') echo "checked='checked'"; ?> <?=permissionChecker('writer_status')?'':'disabled'?>>
                    <label for="myonoffswitch<?=$val->writerID?>" class="onoffswitch-small-label">
                      <span class="onoffswitch-small-inner"></span>
                      <span class="onoffswitch-small-switch <?=permissionChecker('writer_status')?'':'hidden'?>"></span>
                    </label>
                  </div>
                </td>
                <td data-title="<?=$this->lang->line('writer_action')?>">
                  <?=edit_btn('writer_edit',$val->writerID);?>&nbsp;
                  <?=delete_btn('writer_delete',$val->writerID);?>
                </td>
              </tr>
              <?php } } ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </section>
</div>


<div class="modal fade" id="insert" tabindex="-1">
  <div class="modal-dialog modal-sm">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><i class="icofont-plus"></i></span>&nbsp;<?=$this->lang->line('writer_writer')?></h4>
      </div>
      <div class="modal-body">
        <div class="form-group error-name">
          <label><?=$this->lang->line('writer_name')?> <span class='text-danger'>*</span></label>
          <input type="text" class="form-control" name="writer_name" id="writer_name" placeholder="<?=$this->lang->line('writer_name')?>"/>
          <span class="text-red" id="error_writer_name"></span>
        </div>
        <div class="form-group">
          <label><?=$this->lang->line('writer_note')?></label>
          <textarea type="text" name='writer_note' id="writer_note" class="form-control"/ placeholder="<?=$this->lang->line('writer_note')?>"></textarea>
          <span class="text-red" id="error_writer_note"></span>
        </div>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-default insert">Save</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="update" tabindex="-1">
  <div class="modal-dialog modal-sm">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><i class="fa fa-edit"></i></span>&nbsp;<?=$this->lang->line('writer_update')?></h4>
      </div>
      <div class="modal-body">
        <div class="">
          <input type="hidden" class="form-control" name="writerID" id="writerID" readonly/>
          <div class="form-group error-class">
            <label><?=$this->lang->line('writer_name')?> <span class='text-danger'>*</span></label>
            <input type="text" class="form-control" name="writer_name" id="writer_name_up" placeholder="<?=$this->lang->line('writer_name')?>"/>
            <span class="text-red" id="error_writer_name_up"></span>
          </div>
          <div class="form-group">
            <label><?=$this->lang->line('writer_note')?></label>
            <textarea type="text" name='writer_note' id="writer_note_up" class="form-control" placeholder="<?=$this->lang->line('writer_note')?>"></textarea>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-info updated"><?=$this->lang->line('writer_update')?></button>
      </div>
    </div>
  </div>
</div>