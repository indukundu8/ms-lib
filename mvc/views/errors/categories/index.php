<style type="text/css">textarea{resize: none}</style>
<div class="content-wrapper">
  <section class="content-header">
    <h1><?=$this->lang->line('categories_categories')?></h1>
    <ol class="breadcrumb">
      <li><a href="<?=base_url('/')?>"><i class="fa fa-laptop"></i></a></li>
      <li><a href="<?=base_url('/categories')?>"><?=$this->lang->line('categories_categories')?></a></li>
    </ol>
  </section>

  <section class="content">
    <div class="box box-solid">
      <div class="box-header header-custom">
        <h5 class="box-title"><i class="fa fa-list"></i> <?=$this->lang->line('categories_list')?></h5>
        <div class="box-tools pull-right">
          <?=add_btn('categories_add', $this->lang->line('categories_add_categories'));?>
        </div>
      </div>
      <div class="box-body">
        <div id="hide-table">
          <table id="example1" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>#</th>
                <th><?=$this->lang->line('categories_code')?></th>
                <th><?=$this->lang->line('categories_name')?></th>
                <th><?=$this->lang->line('categories_note')?></th>
                <th><?=$this->lang->line('categories_status')?></th>
                <th><?=$this->lang->line('categories_action')?></th>
              </tr>
            </thead>
            <tbody >
              <?php if(count($categories)) { $i=0; foreach($categories as $category) { $i++; ?>
              <tr>
                <td data-title="#"><?=$i?></td>
                <td data-title="<?=$this->lang->line('categories_code')?>"><?=ucfirst($category->categories_code)?></td>
                <td data-title="<?=$this->lang->line('categories_name')?>"><?=ucfirst($category->categories_name)?></td>
                <td data-title="<?=$this->lang->line('categories_note')?>"><?=(!empty($category->categories_note)?$category->categories_note : '&nbsp;')?></td>
                <td data-title="<?=$this->lang->line('categories_status')?>">
                  <div class="onoffswitch-small" id="<?=$category->categoriesID?>">
                    <input type="checkbox" id="myonoffswitch<?=$category->categoriesID?>" class="onoffswitch-small-checkbox" name="categories_status" <?php if($category->categories_status === '1') echo "checked='checked'"; ?> <?=permissionChecker('categories_status')?'':'disabled'?>>
                    <label for="myonoffswitch<?=$category->categoriesID?>" class="onoffswitch-small-label">
                      <span class="onoffswitch-small-inner"></span>
                      <span class="onoffswitch-small-switch <?=permissionChecker('categories_status')?'':'hidden'?>"></span>
                    </label>
                  </div>
                </td>
                <td data-title="<?=$this->lang->line('categories_action')?>">
                  <?=edit_btn('categories_edit',$category->categoriesID);?>&nbsp;
                  <?=delete_btn('categories_delete',$category->categoriesID);?>
                </td>
              </tr>
              <?php } } ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </section>
</div>


<div class="modal fade" id="insert" tabindex="-1">
  <div class="modal-dialog modal-sm">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><i class="icofont-plus"></i></span>&nbsp;<?=$this->lang->line('categories_categories')?></h4>
      </div>
      <div class="modal-body">
        <input type="hidden" class="form-control" name="categories_code" id="categories_code" value="<?=settings()->category_prefixe;categorycode();?>" readonly/>
        <div class="form-group error-name">
          <label><?=$this->lang->line('categories_name')?> <span class='text-danger'>*</span></label>
          <input type="text" class="form-control" name="categories_name" id="categories_name" placeholder="<?=$this->lang->line('categories_name')?>"/>
          <span class="text-red" id="error_categories_name"></span>
        </div>
        <div class="form-group">
          <label><?=$this->lang->line('categories_note')?></label>
          <textarea type="text" name='categories_note' id="categories_note" class="form-control"/ placeholder="<?=$this->lang->line('categories_note')?>"></textarea>
          <span class="text-red" id="error_categories_note"></span>
        </div>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-default insert">Save</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="update" tabindex="-1">
  <div class="modal-dialog modal-sm">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><i class="fa fa-edit"></i></span>&nbsp;<?=$this->lang->line('categories_update')?></h4>
      </div>
      <div class="modal-body">
        <div class="">
          <input type="hidden" class="form-control" name="categoriesID" id="categoriesID" readonly/>
          <div class="form-group error-class">
            <label><?=$this->lang->line('categories_name')?> <span class='text-danger'>*</span></label>
            <input type="text" class="form-control" name="categories_name" id="categories_name_up" placeholder="<?=$this->lang->line('categories_name')?>"/>
            <span class="text-red" id="error_categories_name_up"></span>
          </div>
          <div class="form-group">
            <label><?=$this->lang->line('categories_note')?></label>
            <textarea type="text" name='categories_note' id="categories_note_up" class="form-control" placeholder="<?=$this->lang->line('categories_note')?>"></textarea>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-info updated"><?=$this->lang->line('categories_update')?></button>
      </div>
    </div>
  </div>
</div>