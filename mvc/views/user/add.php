<div class="content-wrapper">
    <section class="content-header">
        <h1><?=$this->lang->line('user_user')?></h1>
        <ol class="breadcrumb">
            <li><a href="<?=base_url('/')?>"><i class="fa fa-laptop"></i></a></li>
            <li><a href="<?=base_url('/user')?>"> <?=$this->lang->line('user_user')?></a></li>
            <li class="active"><?=$this->lang->line('user_add')?></li>
        </ol>
    </section>
    <section class="content">
        <div class="box">

            <div class="row">
                <div class="col-md-12">
                    <form method="post" enctype="multipart/form-data">
                        <div class="box-body">
                            <div class="row">
                                <input type="hidden" name="<?=$this->security->get_csrf_token_name()?>" value="<?=$this->security->get_csrf_hash()?>" />
                                <div class="col-md-3">
                                    <div class="form-group <?=form_error('name') ? 'has-error' : ''?>">
                                        <label for="name"><?=$this->lang->line('user_name')?></label> <span class="text-red">*</span>
                                        <input type="text" class="form-control" id="name" name="name" value="<?=set_value('name')?>" placeholder="<?=$this->lang->line('user_name')?>">
                                        <?=form_error('name','<div class="text-red">', '</div>')?>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group <?=form_error('dob') ? 'has-error' : ''?>">
                                        <label for="dob"><?=$this->lang->line('user_dob')?></label> <span class="text-red">*</span>
                                        <input type="text" class="form-control datepicker" id="dob" name="dob" value="<?=set_value('dob')?>" placeholder="Enter date of birth">
                                        <?=form_error('dob','<div class="text-red">', '</div>')?>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group <?=form_error('gender') ? 'has-error' : ''?>">
                                        <label><?=$this->lang->line('user_gender')?></label> <span class="text-red">*</span>
                                        <?php
                                        $genderArray['0'] = $this->lang->line('user_please_select');
                                        $genderArray['Male'] = $this->lang->line('user_male');
                                        $genderArray['Female'] = $this->lang->line('user_female');
                                        echo form_dropdown('gender', $genderArray,set_value('gender'),'id="gender" class="form-control"');
                                        ?>
                                        <?=form_error('gender','<div class="text-red">', '</div>')?>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group <?=form_error('religion') ? 'has-error' : ''?>">
                                        <label for="religion"><?=$this->lang->line('user_religion')?></label> <span class="text-red">*</span>
                                        <input type="text" class="form-control" id="religion" name="religion" value="<?=set_value('religion')?>" placeholder="Enter religion">
                                        <?=form_error('religion','<div class="text-red">', '</div>')?>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group <?=form_error('email') ? 'has-error' : ''?>">
                                        <label for="email"><?=$this->lang->line('user_email')?></label> <span class="text-red">*</span>
                                        <input type="text" class="form-control" id="email" name="email" value="<?=set_value('email')?>" placeholder="Enter email">
                                        <?=form_error('email','<div class="text-red">', '</div>')?>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group <?=form_error('phone') ? 'has-error' : ''?>">
                                        <label for="phone"><?=$this->lang->line('user_phone')?></label> <span class="text-red">*</span>
                                        <input type="text" class="form-control" id="phone" name="phone" value="<?=set_value('phone')?>" placeholder="Enter phone">
                                        <?=form_error('phone','<div class="text-red">', '</div>')?>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group <?=form_error('jod') ? 'has-error' : ''?>">
                                        <label for="jod"><?=$this->lang->line('user_jod')?></label> <span class="text-red">*</span>
                                        <input type="text" class="form-control datepicker" id="jod" name="jod" value="<?=set_value('jod')?>" placeholder="Enter joinning of date">
                                        <?=form_error('jod','<div class="text-red">', '</div>')?>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group <?=form_error('photo') ? 'has-error' : ''?>">
                                        <label for="photo"><?=$this->lang->line('user_photo')?></label> 
                                        <input type="file" class="form-control" id="photo" name="photo"/>
                                        <?=form_error('photo','<div class="text-red">', '</div>')?>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group <?=form_error('usertypeID') ? 'has-error' : ''?>">
                                        <label for="usertypeID"><?=$this->lang->line('user_role')?></label> <span class="text-red">*</span>
                                        
                                        <?php 
                                        $usertypeArray[0] = "Please Select";
                                        if(count($usertypes)) {
                                          foreach ($usertypes as $usertype) {
                                            $usertypeArray[$usertype->usertypeID] = $usertype->usertype;
                                        }
                                    }
                                    echo form_dropdown('usertypeID', $usertypeArray,set_value('usertypeID'),'id="usertypeID" class="form-control"');
                                    ?>
                                    <?=form_error('usertypeID','<div class="text-red">', '</div>')?>
                                    </div>
                                </div>
                                <div class="col-md-9">
                                    <div class="form-group <?=form_error('address') ? 'has-error' : ''?>">
                                        <label for="address"><?=$this->lang->line('user_address')?></label> <span class="text-red">*</span>
                                        <input name="address" value="<?=set_value('address')?>" id="" class="form-control" placeholder="Enter address">
                                        <?=form_error('address','<div class="text-red">', '</div>')?>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group <?=form_error('status') ? 'has-error' : ''?>">
                                        <label for="status"><?=$this->lang->line('user_status')?></label> <span class="text-red">*</span>
                                        <?php 
                                        $statusArray[0] = $this->lang->line('user_please_select');
                                        $statusArray[1] = $this->lang->line('user_active');
                                        $statusArray[2] = $this->lang->line('user_deactive');
                                        echo form_dropdown('status', $statusArray,set_value('status'),'id="status" class="form-control"');
                                        ?>
                                        <?=form_error('status','<div class="text-red">', '</div>')?>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group <?=form_error('username') ? 'has-error' : ''?>">
                                        <label for="username"><?=$this->lang->line('user_username')?></label> <span class="text-red">*</span>
                                        <input type="text" class="form-control" id="username" name="username" value="<?=set_value('username')?>" placeholder="Enter Username">
                                        <?=form_error('username','<div class="text-red">', '</div>')?>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group <?=form_error('user_password') ? 'has-error' : ''?>">
                                        <label for="password"><?=$this->lang->line('user_password')?></label> <span class="text-red">*</span>
                                        <input type="password" class="form-control" id="password" name="password" value="<?=set_value('password')?>" placeholder="Enter Password">
                                        <?=form_error('password','<div class="text-red">', '</div>')?>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Add User</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
</div>

<script>
$('.datepicker').datepicker({
    autoclose: true,
    format : 'dd-mm-yyyy',
    startView: 2
});
</script>