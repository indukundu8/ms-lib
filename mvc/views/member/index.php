<style type="text/css">
table.dataTable tbody td {vertical-align:middle;}
.col-sm-4{width:32.3333333333%; margin: 1px;}
.custom-barcode-css{border: 1px dotted;padding: 2px;}
.custom-barcode-css p{margin: 0;line-height: 1;font-size: smaller;text-align: center;}
.custom-barcode-css h6{text-align: center;margin: 0;font-weight: bold;}
.custom-barcode-css img{height: 40px;width: 140px;}
</style>
<div class="content-wrapper">
  <section class="content-header">
    <h1><?=$this->lang->line('member_member')?></h1>
    <ol class="breadcrumb">
      <li><a href="<?=base_url('/')?>"><i class="fa fa-laptop"></i></a></li>
      <li><a href="<?=base_url('/member')?>"><?=$this->lang->line('member_member')?></a></li>
    </ol>
  </section>



  <section class="content">
    <div class="box box-solid">
      <div class="box-header header-custom">
        <h5 class="box-title"><i class="fa fa-list"></i> <?=$this->lang->line('member_list')?></h5>
        <div class="box-tools pull-right">
          <?php if(permissionChecker('member_add')){?>
            <a href="<?=base_url('member/add')?>" class="btn btn-inline btn-custom btn-md"><i class="fa fa-plus"></i> <?=$this->lang->line('member_add_member')?></a>
          <?php }?>
        </div>
      </div>
      <div class="box-body">
        <div id="hide-table">
          <table id="example1" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>#</th>
                <th><?=$this->lang->line('member_photo')?></th>
                <th><?=$this->lang->line('member_code')?></th>
                <th><?=$this->lang->line('member_name')?></th>
                <th><?=$this->lang->line('member_parents')?></th>
                <th><?=$this->lang->line('member_phone')?></th>
                <th><?=$this->lang->line('member_occupation')?></th>
                <th><?=$this->lang->line('member_membership')?></th>
                <th><?=$this->lang->line('member_username')?></th>
                <th><?=$this->lang->line('member_status')?></th>
                <th><?=$this->lang->line('member_action')?></th>
              </tr>
            </thead>
            <tbody >
              <?php if(count($member)) { $i=0; foreach($member as $val) { $i++; ?>
              <tr>
                <td data-title="#"><?=$i?></td>
                <td data-title="<?=$this->lang->line('member_photo')?>"><img src="<?=member_img($val->member_photo)?>" class="profile_img" alt="<?=$val->member_name?>"></td>
                <td data-title="<?=$this->lang->line('member_code')?>"><?=ucwords($val->member_code)?></td>
                <td data-title="<?=$this->lang->line('member_name')?>"><?=ucwords($val->member_name)?></td>
                <td data-title="<?=$this->lang->line('member_parents')?>"><?=ucwords($val->member_father_name)?><br><?=ucwords($val->member_mother_name)?></td>
                <td data-title="<?=$this->lang->line('member_phone')?>"><?=ucwords($val->member_phone)?></td>
                <td data-title="<?=$this->lang->line('member_occupation')?>"><?=ucwords($val->member_occupation)?></td>
                <td data-title="<?=$this->lang->line('member_membership')?>"><?=ucwords(isset($membershiptypes[$val->member_membership_type]) ? $membershiptypes[$val->member_membership_type] : '&nbsp;')?></td>
                <td data-title="<?=$this->lang->line('member_username')?>"><?=$val->username?></td>
                <td data-title="<?=$this->lang->line('member_status')?>">
                  <div class="onoffswitch-small" id="<?=$val->memberID?>">
                    <input type="checkbox" id="myonoffswitch<?=$val->memberID?>" class="onoffswitch-small-checkbox" name="categorys_status" <?php if($val->member_status === '1') echo "checked='checked'"; ?><?=permissionChecker('member_status')?'':'disabled'?>>
                    <label for="myonoffswitch<?=$val->memberID?>" class="onoffswitch-small-label">
                      <span class="onoffswitch-small-inner"></span>
                      <span class="onoffswitch-small-switch <?=permissionChecker('member_status')?'':'hidden'?>"></span>
                    </label>
                  </div>
                </td>
                <td data-title="<?=$this->lang->line('member_action')?>">
                  <?=(permissionChecker('member_view')?btn_view_show('member/view/'.base64_encode($val->memberID),$this->lang->line('view')):'');?>
                  <?=(permissionChecker('member_edit')?btn_edit_show('member/edit/'.base64_encode($val->memberID),$this->lang->line('edit')):'');?>
                  <?=changepassword_btn('member_changepassword',$val->memberID); ?>
                  <?=delete_btn('member_delete', $val->memberID); ?>
                </td>
              </tr>
              <?php } } ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </section>
</div>

<!-- change password-->
<div class="modal fade" id="change" tabindex="-1">
  <div class="modal-dialog modal-sm">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><i class="fa fa-key"></i></span>&nbsp;<?=$this->lang->line('member_change_password')?></h4>
      </div>
      <div class="modal-body">
        <div class="form-group error-name">
          <label><?=$this->lang->line('member_new_password')?> <span class='text-danger'>*</span></label>
          <input type="password" class="form-control" name="new_password" id="new_password" data-toggle="password" placeholder="<?=$this->lang->line('user_new_password')?>"/>
          <span class="text-red" id="error_new_password"></span>
        </div>
        <div class="form-group error-percentage">
          <label><?=$this->lang->line('member_confirm_password')?> <span class='text-danger'>*</span></label>
          <input type="password" name='confirm_password' id="confirm_password" class="form-control" data-toggle="password" placeholder="<?=$this->lang->line('user_confirm_password')?>"/>
          <span class="text-red" id="error_confirm_password"></span>
        </div>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-default changepassword">Save</button>
      </div>
    </div>
  </div>
</div>

<!-- View barcode   -->
<div class="modal fade" id="barcode" tabindex="-1">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><i class="fa fa-barcode"></i>&nbsp; Barcode</h4>
      </div>

      <div class="modal-body">
        <table  class="table dt-responsive zero-border" id='print_areas'>
          <tbody id="showBarcode">
          </tbody>
        </table>
      </div>
      <div class="modal-footer">
        <button class="btn btn-sm btn-info prints" id="get_print"><span class="glyphicon glyphicon-print" ></span>&nbsp;Print</button>
        <button type="button" class="btn btn-default " data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>