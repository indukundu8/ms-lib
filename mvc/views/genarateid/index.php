<style type="text/css">
  .select2-selection__choice{background-color: #524E8F !important}
  .box-border{border:1px solid#ccc}
  .img-height{height: 44px; position: absolute;top: 0px;right: 1px;}
  .idcard-img-resize{padding: 0px !important;}
  .idcard-font-resize h4{line-height: 0px;margin-bottom: 15px;font-size: 15px;}
  .idcard-font-resize p{font-size: 10px;}
  .idcard-font-resize img{position: absolute;bottom: 5px; right: 0px;}
  .idcard-img-resize p{padding-top: 10px;}
</style>
<div class="content-wrapper">
  <section class="content-header">
    <h1><?=$this->lang->line('genarateid_genarateid')?></h1>
    <ol class="breadcrumb">
      <li><a href="<?=base_url('/')?>"><i class="fa fa-laptop"></i></a></li>
      <li><a href="<?=base_url('/genarateid')?>"><?=$this->lang->line('genarateid_genarateid')?></a></li>
    </ol>
  </section>
  <section class="content">
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title"><?=$this->lang->line('genarateid_generate')?></h3>
      </div>
      <form method="post" enctype="multipart/form-data">
        <div class="box-body">
          <div class="row">
            <input type="hidden" name="<?=$this->security->get_csrf_token_name()?>" value="<?=$this->security->get_csrf_hash()?>" />
            <div class="col-md-8">
              <div class="form-group <?=form_error('memberid[]') ? 'has-error' : ''?>">
                <label for="genarateid_label"><?=$this->lang->line('genarateid_label')?>&nbsp;<i class="fa fa-question-circle" data-toggle="tooltip" data-placement="right" title="<?=$this->lang->line('genarateid_tooltip')?>"></i></label> <span class="text-red">*</span>
                <?php 
                if(count($genarateid)){
                  foreach ($genarateid as $val){
                    $array[$val->memberID] = ucwords($val->member_name);
                  }
                }
                echo form_dropdown('memberid[]', (count($genarateid)) ? $array:'', set_value('memberid'),'id="memberid[]" class="form-control select2" multiple="multiple"');
                ?>  
                <?=form_error('memberid[]','<div class="text-red">', '</div>')?>           
              </div>
            </div>
          <div class="col-md-4">
            <label for="submit">&nbsp;</label>
            <button type="submit" class="btn btn-primary btn-flat btn-block"><?=$this->lang->line('genarateid_generate')?></button>
          </div>
        </div><br>
      </div>
    </form>
  </div>



  <?php if($find_data == 'true'){?>
    <div class="box box-solid">


      <div class="box-header header-custom">
        <h5 class="box-title"><i class="fa fa-list"></i> <?=$this->lang->line('genarateid_details')?></h5>
        <div class="box-tools pull-right">
            <?php if(permissionChecker('genarateid_print')){?>
              <form action="<?=base_url('genarateid/print')?>" method="post">
                <input type="hidden" name="<?=$this->security->get_csrf_token_name()?>" value="<?=$this->security->get_csrf_hash()?>" />
                <?php foreach ($ids as $ids){?><input type="hidden" name="ids[]" id="ids[]" value="<?=$ids?>" /><?php } ?>
                <button type="submit" class="btn btn-custom btn-flat btn-block"><i class="fa fa-print"></i>&nbsp;<?=$this->lang->line('genarateid_print')?></button>
              </form>
            <?php } ?>
        
        </div>
      </div>
      <div class="box-body" id='print_areas'>
          <div class="row">
            <?php foreach($member_details as $val){ ?>
              <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                <div class="box box-border">
                  <div class="box-header with-border">
                    <h3 class="box-title"><?=settings()->company_name?></h3>
                    <img class="img-responsive img-height" src="<?=base_url('uploads/images/'.settings()->logo)?>">
                  </div>
                  <div class="box-body" >
                    <div class="col-lg-4 col-sm-12 col-md-12 col-xs-3 idcard-img-resize">
                      <img src="<?=member_img($val->member_photo)?>" class="img-thumbnail" alt="<?=$val->member_name?>">
                      <p align="center"><b><?=$val->member_code;?></b></p>
                    </div>
                    <div class="col-lg-8 col-sm-12 col-md-12 col-xs-9 idcard-font-resize">
                      <p><?=$this->lang->line('genarateid_name')?></p>
                      <h4><b><?= ucwords($val->member_name)?></b></h4>

                      <p><?=$this->lang->line('genarateid_membership')?></p>
                      <h4><b><?= ucwords($membership[$val->member_membership_type])?></b></h4>

                      <p><?=$this->lang->line('genarateid_gender')?></p>
                      <h4><b><?= ucwords($val->member_gender)?></b></h4>


                      <p><?=$this->lang->line('genarateid_joined')?></p>
                      <h4><b><?= app_date($val->member_since_date)?></b></h4>
                      <img src="<?=base_url('genarateid/qrcode/'.$val->member_code);?>">
                    </div>
                  </div>
                </div>
              </div>
            <?php } ?> 
          </div>
      </div>
    </div>
  <?php }?>
</section>
</div>


