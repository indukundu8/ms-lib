<?php 

$lang['categories_categories']     = "শ্রেণিবদ্ধ";
$lang['categories_add_categories']     = "শ্রেণিবদ্ধ সংযোজন";
$lang['categories_list']     = "তালিকা";

$lang['categories_code']       = "কোড";
$lang['categories_name']       = "নাম";
$lang['categories_note']  	  = "নোট";
$lang['categories_status']     = "অবস্থা";
$lang['categories_action'] 	  = "ক্রিয়া";


$lang['categories_insert'] = "ইনসার্ট";
$lang['categories_update'] = "আপডেট";

?>