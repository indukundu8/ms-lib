<?php 

$lang['wastage_wastage']     = "অপচয়";
$lang['wastage_add_wastage']     = "অপচয় সংযোজন";
$lang['wastage_list']     = "তালিকা";

$lang['wastage_photo']       = "ছবি";
$lang['wastage_book_code']       = "বইয়ের কোডe";
$lang['wastage_book_select']       = "বই নির্বাচন করুন";
$lang['wastage_book_name']  	  = "বইয়ের নাম";
$lang['wastage_writer_name']     = "লেখকের নাম";
$lang['wastage_wastager_by']     = "অপচয়ের মাধ্যম";
$lang['wastage_date']     = "তারিখ";
$lang['wastage_payable_amount']     = "অপচয়ের আর্থিক পরিমান";
$lang['wastage_note']     = "নোট";
$lang['wastage_action'] 	  = "ক্রিয়া";


$lang['wastage_insert'] = "ইনসার্ট";
$lang['wastage_update'] = "আপডেট";

?>