<?php 

$lang['book_book']     			= "বই";
$lang['book_add_book']     		= "বই সংযোজন";
$lang['book_update']     		= "হালনাগাদ";
$lang['book_add']     			= "সংযোজন";
$lang['book_please_select']     = "নির্বাচন";
$lang['book_info']     			= "বইয়ের তথ্য";
$lang['book_list']     			= "তালিকা";
$lang['book_name']     			= "নাম";
$lang['book_categories']     	= "শ্রেণিবদ্ধ";
$lang['book_publication']     	= "প্রকাশনা";
$lang['book_isbn']     			= "ISBN";
$lang['book_writer']     		= "লেখক";
$lang['book_edition']     		= "সংস্করণ";
$lang['book_edition_year']     	= "সংস্করণ বর্ষ";
$lang['book_photo']     		= "ছবি";
$lang['book_price']     		= "মূল্য";
$lang['book_quantity'] 			= "পরিমাণ";
$lang['book_availability'] 		= "প্রাপ্যতা";
$lang['book_rack_no']    		= "তাক নাম্বার";
$lang['book_purchase_price']    = "ক্রয় মূল্য";

$lang['book_details']     		= "বিশদ বিবরণ";
$lang['book_entry']     		= "বই এন্ট্রি";


$lang['book_code']     			= "কোড";
$lang['book_status']     		= "অবস্থা";
$lang['book_action'] 			= "ক্রিয়া";
$lang['book_code_tooltip']		= "আপনার লাইব্রেরির জন্য আপনার নিজের কোড ব্যবহার করার সুযোগ রয়েছে";
$lang['book_issued_quantity_tooltip']		= "সদস্যদের জন্য জারি করা বইয়ের সংখ্যা";
$lang['book_issued_quantity']		= "জারি করা বইয়ের সংখ্যা";
$lang['isbn_tooltip']			= "International Standard Book Number";


?>