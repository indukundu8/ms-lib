<?php 

$lang['panel_title']    = 'ইমেল সেটিং';
$lang['emailsetting_select']    = 'ইমেল সেটিং নির্বাচন';
$lang['emailsetting_email_setting']    = 'SMTP কনফিগারেশন';
$lang['emailsetting_email_engine']     = 'ইমেল ইঞ্জিন';
$lang['emailsetting_send_mail']        = 'মেইল প্রেরণ';
$lang['emailsetting_smtp']             = 'SMTP';
$lang['emailsetting_smtp_username']    = 'SMTP ইউজারনেম';
$lang['emailsetting_smtp_password']    = 'SMTP পাসওয়ার্ড';
$lang['emailsetting_smtp_server']      = 'SMTP সার্ভার';
$lang['emailsetting_smtp_port']        = 'SMTP পোর্ট';
$lang['emailsetting_smtp_security']    = 'SMTP সিকিউরিটি';
$lang['emailsetting_sendmail_email']   = 'ইমেল';
$lang['update_emailsetting']    = 'আপডেট';