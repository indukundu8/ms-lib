<?php 

$lang['user_user']     = "المستعمل";
$lang['user_add_user']     = "إضافة مستخدم";
$lang['user_add']     = "يضيف";
$lang['user_list']     = "قائمة";
$lang['user_name']     = "اسم";
$lang['user_dob']      = "تاريخ الولادة";
$lang['user_gender']   = "جنس";
$lang['user_please_select']   = "الرجاء التحديد";
$lang['user_male']   = "ذكر";
$lang['user_female']   = "أنثى";
$lang['user_religion'] = "دين";
$lang['user_email']    = "بريد إلكتروني";
$lang['user_phone']    = "هاتف";
$lang['user_address']  = "عنوان";
$lang['user_jod']      = "تاريخ الالتحاق";
$lang['user_photo']    = "صورة";
$lang['user_active']   = "نشيط";
$lang['user_usertypeID'] = "هوية نوع المستخدم";
$lang['user_username'] = "اسم المستخدم";
$lang['user_password'] = "كلمه السر";
$lang['user_role'] 	= "دور";
$lang['user_status'] = "حالة";
$lang['user_active'] = "تفعيل";
$lang['user_deactive'] = "تعطيل";
$lang['user_action'] = "عمل";
$lang['user_update'] = "تحديث";
$lang['user_profile'] = "الملف الشخصي";
$lang['user_change_password'] = "تغيير كلمة المرور";
$lang['user_new_password'] = "كلمة السر الجديدة";
$lang['user_confirm_password'] = "تأكيد كلمة المرور";

?>