<?php 

$lang['writer_writer']     = "كاتب";
$lang['writer_add_writer']     = "اضافة كاتب";
$lang['writer_list']     = "قائمة";

$lang['writer_name']       = "اسم";
$lang['writer_note']  	  = "ملحوظة";
$lang['writer_status']     = "حالة";
$lang['writer_action'] 	  = "عمل";


$lang['writer_insert'] = "إدراج";
$lang['writer_update'] = "تحديث";

?>