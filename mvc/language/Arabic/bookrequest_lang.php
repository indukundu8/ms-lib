<?php 

$lang['bookrequest_bookrequest']     	= "طلب كتاب";
$lang['bookrequest_add_bookrequest']    = "الكتاب المطلوب";
$lang['bookrequest_list']     			= "قائمة";
$lang['bookrequest_name']       		= "اسم الكتاب";
$lang['bookrequest_writer_name']       	= "اسم الكاتب";
$lang['bookrequest_categories']       	= "فئات";
$lang['bookrequest_edition']       		= "الإصدار";
$lang['bookrequest_note']  	  			= "ملحوظة";
$lang['bookrequest_member']  	  		= "اسم عضو";
$lang['bookrequest_action'] 	  		= "عمل";
$lang['bookrequest_insert'] 			= "إدراج";
$lang['bookrequest_update'] 			= "تحديث";

?>