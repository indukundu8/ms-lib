<?php 

$lang['import_import']     			= "CSV فائل درآمد کریں۔";
$lang['import_list']     			= "CSV فارمیٹ کے ذریعے فائل درآمد کریں۔";
$lang['import_note']     			= "اہم نوٹ";
$lang['download']     				= "ڈاؤن لوڈ کریں";
$lang['select_file']     			= "CSV فائل منتخب کریں۔";
$lang['upload_btn']     			= "CSV درآمد کریں۔";
$lang['import_note_1']     			= "CSV فائل فارمیٹ ڈاؤن لوڈ کریں۔";
$lang['import_note_2']     			= "CSV فائل کا ٹیبل ٹائٹل تبدیل نہیں کیا جا سکتا۔";
$lang['import_note_3']     			= "کتاب کوڈ کے سابقہ ​​کو صحیح طریقے سے استعمال کریں۔";
$lang['import_note_4']     			= "کتاب کیٹیگری آئی ڈی درست ہونی چاہیے۔";
$lang['import_note_5']     			= "کتاب لکھنے والے کی شناخت درست ہونی چاہیے۔";
$lang['import_note_6']     			= "کتاب کی اشاعت کی شناخت درست ہونی چاہیے۔";

?>