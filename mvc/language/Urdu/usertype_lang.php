<?php 

$lang['usertype']     = "صارف کا کردار";
$lang['usertype_list']     = "فہرست";
$lang['usertype_add_usertype']     = "صارف کا کردار شامل کریں";
$lang['usertype_action'] = "عمل";
$lang['usertype_update'] = "اپ ڈیٹ";
$lang['usertype_add']     = "شامل کریں";
$lang['usertype_name']     = "نام";

$lang['user_dob']      = "پیدائش کی تاریخ";
$lang['user_gender']   = "صنف";
$lang['user_please_select']   = "براہ مہربانی منتخب کریں";
$lang['user_male']   = "مرد";
$lang['user_female']   = "عورت";
$lang['user_religion'] = "مذہب";
$lang['user_email']    = "ای میل";
$lang['user_phone']    = "فون";
$lang['user_address']  = "پتہ";
$lang['user_jod']      = "شمولیت کی تاریخ";
$lang['user_photo']    = "تصویر";
$lang['user_active']   = "فعال";
$lang['user_usertypeID'] = "یوزر ٹائپ ایڈ";
$lang['user_username'] = "صارف نام";
$lang['user_password'] = "پاس ورڈ";
$lang['user_role'] 	= "کردار";
$lang['user_status'] = "حالت";
$lang['user_active'] = "محرک کریں";
$lang['user_deactive'] = "غیر فعال کریں";
$lang['user_profile'] = "پروفائل";
$lang['user_change_password'] = "پاس ورڈ تبدیل کریں";
$lang['user_new_password'] = "نیا پاس ورڈ";
$lang['user_confirm_password'] = "پاس ورڈ کی تصدیق کریں";

?>