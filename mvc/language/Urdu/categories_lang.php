<?php 

$lang['categories_categories']     = "اقسام";
$lang['categories_add_categories']     = "زمرہ شامل کریں";
$lang['categories_list']     = "فہرست";

$lang['categories_code']       = "کوڈ";
$lang['categories_name']       = "نام";
$lang['categories_note']  	  = "نوٹ";
$lang['categories_status']     = "حالت";
$lang['categories_action'] 	  = "عمل";

$lang['categories_insert'] = "داخل کریں";
$lang['categories_update'] = "اپ ڈیٹ";

?>