<?php 
$lang['add']      = "شامل کریں";
$lang['edit']     = "ترمیم";
$lang['view']     = "دیکھیں";
$lang['delete']   = "حذف کریں";

$lang['menu_dashboard']     = "ڈیش بورڈ";
$lang['menu_user']          = "صارفین";
$lang['menu_newuser']          = "صارف";
$lang['menu_usertype']      = "صارف کا کردار";

$lang['menu_permissions']   = "اجازت";
$lang['menu_permissionlog'] = "اجازت بلاگ";
$lang['menu_permissionmodule'] = "اجازت ماڈیول";
$lang['menu_menu']             = "مینو";

$lang['menu_settings']  = "ترتیبات";
$lang['menu_sitesettings']  = "کمپنی پروفائل";
$lang['menu_membership']  = "ممبرشپ";
$lang['menu_member']  = "رکن";
$lang['menu_memberlist']  = "ممبر کی فہرست";
$lang['menu_categories']  = "اقسام";
$lang['menu_book']  = "کتاب";
$lang['menu_book_archive']  = "محفوظ شدہ دستاویزات";
$lang['menu_wastage']  = "بربادی";
$lang['menu_publication']  = "اشاعت";
$lang['menu_circulation']  = "گردش";
$lang['menu_payment']  = "ادائیگی";
$lang['menu_report']  = "رپورٹ کریں";
$lang['menu_writer']  = "لکھاری";
$lang['menu_emailsetting']  = "ای میل کی تشکیل";
$lang['menu_bookrequest']  = "کتاب کی درخواست";
$lang['menu_genarateid']  = "صنف شناختی کارڈ";
$lang['menu_import']  = "CSV درآمد کریں۔";

?>