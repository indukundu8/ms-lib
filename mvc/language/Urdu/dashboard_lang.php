<?php 

$lang['dashboard_dashboard']     = "ڈیش بورڈ";
$lang['dashboard_more']  = "مزید معلومات";
$lang['dashboard_member']     = "ممبران";
$lang['dashboard_book'] 	  = "کتابیں";
$lang['dashboard_categories'] 	  = "اقسام";
$lang['dashboard_membership'] = "ممبرشپ";
$lang['dashboard_issued'] = "جاری کردیا گیا";
$lang['dashboard_wastage'] = "بربادی";
$lang['dashboard_publication'] = "اشاعت";
$lang['dashboard_writer'] = "لکھاری";
$lang['dashboard_bar_chat'] = "جاری اور واپس شدہ بار چارٹ";
$lang['dashboard_issued_book'] = "آخری 5 جاری کردہ کتاب";
$lang['dashboard_returned_book'] = "آخری 5 لوٹائی گئی کتاب";

$lang['circulation_member_name'] = "رکن کا نام";
$lang['circulation_book_name'] = "کتاب کا نام";
$lang['circulation_writer_name'] = "مصنف کا نام";
$lang['circulation_issue_date'] = "تاریخ اجراء";
$lang['circulation_expiry_date'] = "واپسی کی آخری تاریخ";
$lang['circulation_return_date'] = "واپسی کی تاریخ";

$lang['categories_code'] = "کوڈ";
$lang['categories_name'] = "نام";

?>