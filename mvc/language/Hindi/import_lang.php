<?php 

$lang['import_import']     			= "सीएसवी फ़ाइल आयात करें";
$lang['import_list']     			= "CSV प्रारूप द्वारा फ़ाइल आयात करें";
$lang['import_note']     			= "महत्वपूर्ण लेख";
$lang['download']     				= "डाउनलोड";
$lang['select_file']     			= "सीएसवी फ़ाइल का चयन करें";
$lang['upload_btn']     			= "सीएसवी आयात करें";
$lang['import_note_1']     			= "CSV फ़ाइल प्रारूप डाउनलोड करें";
$lang['import_note_2']     			= "CSV फ़ाइल का तालिका शीर्षक बदला नहीं जा सकता";
$lang['import_note_3']     			= "पुस्तक कोड के उपसर्ग का सही प्रयोग करें";
$lang['import_note_4']     			= "पुस्तक श्रेणी आईडी सही होनी चाहिए";
$lang['import_note_5']     			= "बुक राइटर आईडी सही होना चाहिए";
$lang['import_note_6']     			= "पुस्तक प्रकाशन आईडी सही होनी चाहिए";

?>