<?php 

$lang['categories_categories']     = "श्रेणियाँ";
$lang['categories_add_categories']     = "श्रेणी जोड़ना";
$lang['categories_list']     = "List";

$lang['categories_code']       = "सूची";
$lang['categories_name']       = "नाम";
$lang['categories_note']  	  = "टिप्पणी";
$lang['categories_status']     = "स्थिति";
$lang['categories_action'] 	  = "कार्य";


$lang['categories_insert'] = "डालने";
$lang['categories_update'] = "अपडेट करें";

?>