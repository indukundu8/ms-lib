<?php 

$lang['writer_writer']     = "लेखक";
$lang['writer_add_writer']     = "लेखक जोड़ें";
$lang['writer_list']     = "सूची";

$lang['writer_name']       = "नाम";
$lang['writer_note']  	  = "ध्यान दें";
$lang['writer_status']     = "स्थिति";
$lang['writer_action'] 	  = "कार्य";

$lang['writer_insert'] = "डालने";
$lang['writer_update'] = "अपडेट करें";

?>