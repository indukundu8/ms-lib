<?php 

$lang['usertype']     = "User Role";
$lang['usertype_list']     = "List";
$lang['usertype_add_usertype']     = "Add User Role";
$lang['usertype_action'] = "Action";
$lang['usertype_update'] = "Update";
$lang['usertype_add']     = "Add";
$lang['usertype_name']     = "Name";

$lang['user_dob']      = "Date of birth";
$lang['user_gender']   = "Gender";
$lang['user_please_select']   = "Please Select";
$lang['user_male']   = "Male";
$lang['user_female']   = "Female";
$lang['user_religion'] = "Religion";
$lang['user_email']    = "Email";
$lang['user_phone']    = "Phone";
$lang['user_address']  = "Address";
$lang['user_jod']      = "Date of Joining";
$lang['user_photo']    = "Photo";
$lang['user_active']   = "Active";
$lang['user_usertypeID'] = "UsertypeID";
$lang['user_username'] = "Username";
$lang['user_password'] = "Password";
$lang['user_role'] 	= "Role";
$lang['user_status'] = "Status";
$lang['user_active'] = "Activate";
$lang['user_deactive'] = "Deactivate";
$lang['user_profile'] = "Profile";
$lang['user_change_password'] = "Change Password";
$lang['user_new_password'] = "New Password";
$lang['user_confirm_password'] = "Confirm Password";

?>