<?php 
$lang['member_member']     			= "Member";
$lang['member_add_member']     		= "Add Member";
$lang['member_profile']     		= "Member Profile";
$lang['member_update']     			= "Update";
$lang['member_add']     			= "Add";
$lang['member_please_select']     	= "Please Select";
$lang['member_info']     			= "Member Info";
$lang['member_list']     			= "List";
$lang['member_name']     			= "Name";
$lang['member_father_name']     	= "Father Name";
$lang['member_mother_name']     	= "Mother Name";
$lang['member_date_of_birth']     	= "Date of Birth";
$lang['member_nid_no']     			= "NID No.";
$lang['member_nid_no_tooltip']     	= "National Identification Number";
$lang['member_membership']     		= "Membership";
$lang['member_membership_tooltip']	= "Select the type of membership you want";
$lang['member_occupation']     		= "Occupation";
$lang['member_parents']     		= "Parents";
$lang['member_gender']     			= "Gender";
$lang['member_male']     			= "Male";
$lang['member_female']     			= "Female";
$lang['member_third_gender']     	= "Others";
$lang['member_blood_group']     	= "Blood Group";
$lang['member_religion']     		= "Religion";
$lang['member_phone']     			= "Contact No.";
$lang['member_email']     			= "Email";
$lang['member_since']     			= "Registered Date";
$lang['member_address']     		= "Address";
$lang['member_photo']     			= "Photo";
$lang['member_change_password']     = "Change Password";
$lang['member_new_password']     	= "New Password";
$lang['member_confirm_password']    = "Confirm Password";
$lang['member_login_info']     		= "Login Info";
$lang['member_username']     		= "Username";
$lang['member_password']     		= "Password";
$lang['member_confirm_password']    = "Confirm Password";
$lang['member_code']     			= "Code";
$lang['member_status']     			= "Status";
$lang['member_action'] 				= "Action";
$lang['member_details_list'] 		= "Details";
$lang['member_circulation']			= "Circulation Details";
$lang['member_payment_details']		= "Payment Details";

$lang['circulation_book_code']		= "Book Code";
$lang['circulation_book_name']		= "Book Name";
$lang['circulation_writer_name']	= "Writer Name";
$lang['circulation_issue_date']		= "Issued Date";
$lang['circulation_expiry_date']	= "Last Date to return";
$lang['circulation_return_date']	= "Returned Date";
$lang['circulation_penalty']		= "Penalty";
$lang['circulation_return_status']	= "Status";

$lang['payment_for']				= "Payment For";
$lang['payment_amount']				= "Amount";
$lang['payment_date']				= "Date";



?>