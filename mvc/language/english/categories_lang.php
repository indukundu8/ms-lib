<?php 

$lang['categories_categories']     = "Categories";
$lang['categories_add_categories']     = "Add Category";
$lang['categories_list']     = "List";

$lang['categories_code']       = "Code";
$lang['categories_name']       = "Name";
$lang['categories_note']  	  = "Note";
$lang['categories_status']     = "Status";
$lang['categories_action'] 	  = "Action";


$lang['categories_insert'] = "Insert";
$lang['categories_update'] = "Update";

?>