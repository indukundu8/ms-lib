<?php 

$lang['login_username']    = "Username";
$lang['login_password']    = "Password";
$lang['login_sign_in']     = "Sign In";
$lang['retry']     = "Retry Login";
$lang['login_remember_me'] = "Remember Me";
$lang['forgot_password'] = "I forgot my password";
$lang['forgot_password_header'] = "Forgot Password";
$lang['forgot_password_sms'] = "Reset Your Password Here";
$lang['forgot_email_address'] = "Email Address";
$lang['reset_password'] = "Reset Password";
$lang['reset_password_sms'] = "Set Your New Password";
$lang['new_password'] = "New Password";
$lang['confirm_new_password'] = "Confirm New Password";
$lang['validation'] = "Verification Process";
$lang['validation_code'] = "verification Code";
$lang['validation_sms'] = "Please copy the validation code from the e-mail and paste it here";
$lang['verify_button'] = "Verify";
$lang['check_validation_code'] = "Validation Code is Invalid";

?>