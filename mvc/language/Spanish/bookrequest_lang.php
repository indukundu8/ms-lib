<?php 

$lang['bookrequest_bookrequest']     	= "Solicitud de reserva";
$lang['bookrequest_add_bookrequest']    = "Libro solicitado";
$lang['bookrequest_list']     			= "Lista";
$lang['bookrequest_name']       		= "Nombre del libro";
$lang['bookrequest_writer_name']       	= "Nombre del escritor";
$lang['bookrequest_categories']       	= "Categorías";
$lang['bookrequest_edition']       		= "Edición";
$lang['bookrequest_note']  	  			= "Nota";
$lang['bookrequest_member']  	  		= "Nombre de miembro";
$lang['bookrequest_action'] 	  		= "Acción";
$lang['bookrequest_insert'] 			= "Insertar";
$lang['bookrequest_update'] 			= "Actualizar";

?>