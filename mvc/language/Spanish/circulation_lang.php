<?php 

$lang['circulation_circulation']     = "Circulación";
$lang['circulation_add_circulation']     = "añadir categoría";
$lang['circulation_issue_and_return']     = "Emitido y devuelto";
$lang['circulation_list']     = "Lista";

$lang['circulation_member_code'] = "Código de miembro";
$lang['circulation_member_id'] = "Identificación de miembro";
$lang['circulation_member_name'] = "Nombre de miembro";
$lang['circulation_book_name'] = "Nombre del libro";
$lang['circulation_book_code'] = "Código del libro";
$lang['circulation_book_code_tooltip'] = "Pon tu código de libro";
$lang['circulation_writer_name'] = "Nombre del escritor";
$lang['circulation_issue_date'] = "Fecha de asunto";
$lang['circulation_expiry_date'] = "Última fecha de regreso";
$lang['circulation_return_date'] = "Fecha de regreso";
$lang['circulation_return_status'] = "Estado de devolución";
$lang['circulation_penalty'] = "Multa";
$lang['circulation_penalty_fee'] = "tarifas de penalización";
$lang['circulation_no_of_days'] = "número de días";
$lang['circulation_penalty_message'] = "Esta fecha de devolución de libros ha terminado. Pague la multa.";
$lang['circulation_penalty_note'] = "Nota";
$lang['member_penalty_amount'] = "Cantidad total";
$lang['circulation_action'] 	  = "Acción";

$lang['circulation_insert'] = "Insertar";
$lang['circulation_update'] = "Actualizar";
$lang['circulation_search'] = "Buscar";
$lang['circulation_details'] = "Detalles";
$lang['circulation_issue'] = "Libro de Ediciones";
$lang['circulation_issued'] = "Asunto";
$lang['circulation_not_available'] = "No disponible";
$lang['circulation_member_search'] = "Buscar miembro";

$lang['circulation_member_gender'] = "Género";
$lang['circulation_member_occupation'] = "Ocupación";
$lang['circulation_member_phone'] = "Teléfono";
$lang['circulation_book_return'] = "Regreso";
$lang['circulation_book_renewal'] = "Renovación";
$lang['circulation_book_lost'] = "perdido";
$lang['circulation_book_lost_message'] = "Dado que perdió el libro, debe pagar el precio del libro.";
$lang['circulation_book_price'] = "Precio del libro";
$lang['circulation_book_payable_amount'] = "Cantidad a pagar";

$lang['book_photo'] = "Foto";
$lang['book_code'] = "Código";
$lang['book_name'] = "Nombre";
$lang['book_writer'] = "el autor";
$lang['book_edition'] = "Edición";
$lang['book_price'] = "Precio";
$lang['book_availability'] = "Disponibilidad";
$lang['book_rack_no'] = "Número de rack";
$lang['book_action'] = "Acción";

?>