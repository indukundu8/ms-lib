<?php 

$lang['login_username']    = "Nombre de usuario";
$lang['login_password']    = "Contraseña";
$lang['login_sign_in']     = "Iniciar sesión";
$lang['retry']     = "Volver a intentar iniciar sesión";
$lang['login_remember_me'] = "guardar la información de inicio de sesión";
$lang['forgot_password'] = "Olvidé mi contraseña";
$lang['forgot_password_header'] = "Has olvidado tu contraseña";
$lang['forgot_password_sms'] = "Restablecer su contraseña aquí";
$lang['forgot_email_address'] = "Dirección de correo electrónico";
$lang['reset_password'] = "Restablecer la contraseña";
$lang['reset_password_sms'] = "Establezca su nueva contraseña";
$lang['new_password'] = "Nueva contraseña";
$lang['confirm_new_password'] = "Confirmar nueva contraseña";
$lang['validation'] = "Proceso de verificación";
$lang['validation_code'] = "código de verificación";
$lang['validation_sms'] = "Copie el código de validación del correo electrónico y péguelo aquí";
$lang['verify_button'] = "Verificar";
$lang['check_validation_code'] = "El código de validación no es válido";

?>