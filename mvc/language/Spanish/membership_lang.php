<?php 

$lang['membership_membership']     = "Afiliación";
$lang['membership_add_membership']     = "Agregar membresía";
$lang['membership_list']     = "Lista";

$lang['membership_code']      = "Código";
$lang['membership_name']      = "Nombre";
$lang['membership_limit_book']  = "Límite de libros";
$lang['membership_limit_day']  = "Límite de días";
$lang['membership_fee']  = "Cuota de socio";
$lang['membership_penalty_fee']  = "Multa";
$lang['membership_renew_limit']  = "Límite de renovación";
$lang['membership_free']  = "Libre";
$lang['membership_status']     = "Estado";
$lang['membership_action'] 	  = "Acción";

$lang['membership_add_to_cart'] = "Añadir al carrito";
$lang['membership_insert'] = "Insertar";
$lang['membership_error'] = "Algo está mal";
$lang['membership_update'] = "Actualizar";

$lang['membership_note'] = "Nota:";
$lang['membership_penalty_note'] = "1. Se agregará una penalización según el día.";
$lang['membership_renew_note'] = "2. El límite de renovación se aplica al libro.";

?>