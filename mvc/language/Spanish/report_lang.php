<?php 
$lang['reports_reports']     	= "Informes";
$lang['reports_member']     	= "Informe de miembros";
$lang['reports_book']     		= "Informe del libro";
$lang['reports_payment']     	= "Informe de pago";
$lang['reports_categories']     = "Informe de categorías";
$lang['reports_details']     	= "Detalles aquí";
$lang['reports_date']     		= "Fecha";
$lang['report_print']     		= "Impresión";
$lang['report_generated']     	= "Generar informe";
$lang['report_generate']     	= "Generar";
$lang['report_status']     		= "Estado";

$lang['member_name']     		= "Nombre";
$lang['member_type']     		= "Tipo";
$lang['member_occupation']     	= "Ocupación";
$lang['member_parents']     	= "Padre madre";
$lang['member_phone']     		= "Número de contacto";
$lang['member_address']     	= "Habla a";
$lang['report_photo']     		= "Foto";
$lang['report_code']     		= "Código";
$lang['member_nid']     		= "Identidad nacional";
$lang['report_remark'] 			= "Observación";

$lang['reports_please_select']     = "Por favor seleccione";
$lang['reports_membership_type']   = "tipo de membresía";
$lang['reports_member_Active']     = "Activo";
$lang['reports_member_Inactive']   = "Inactivo";

$lang['reports_categories_type']   = "Categorías";

$lang['book_name']     			= "Nombre";
$lang['book_writer']     		= "el autor";
$lang['book_edition']     		= "Edición";
$lang['book_quantity'] 			= "Cantidad";
$lang['book_availability'] 		= "Disponibilidad";
$lang['book_rack_no']    		= "Número de rack";
$lang['categories_name']     = "Nombre";
$lang['categories_book']     = "Libros";

$lang['report_payment']     = "Informe de pago";
$lang['reports_from_date']     = "Partir de la fecha";
$lang['reports_to_date']     = "Hasta la fecha";
$lang['reports_for']     = "Para";
$lang['reports_lost_book']     = "Tarifa de libro perdido";
$lang['reports_expiry_date']     = "la tarifa por la fecha de devolución falla";
$lang['reports_membership_fee']     = "Cuota de socio";
$lang['payment_member_name']    = "Nombre de miembro";
$lang['payment_for']     		= "Pago por";
$lang['payment_amount']     	= "Cantidad a pagar";
$lang['payment_date']     		= "Fecha";

?>