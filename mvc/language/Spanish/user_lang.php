<?php 

$lang['user_user']     = "Usuario";
$lang['user_add_user']     = "Agregar usuario";
$lang['user_add']     = "Agregar";
$lang['user_list']     = "Lista";
$lang['user_name']     = "Nombre";
$lang['user_dob']      = "Fecha de nacimiento";
$lang['user_gender']   = "Género";
$lang['user_please_select']   = "Por favor seleccione";
$lang['user_male']   = "Masculino";
$lang['user_female']   = "Masculina";
$lang['user_religion'] = "Religión";
$lang['user_email']    = "Correo electrónico";
$lang['user_phone']    = "Teléfono";
$lang['user_address']  = "Habla a";
$lang['user_jod']      = "Fecha de inscripción";
$lang['user_photo']    = "Foto";
$lang['user_active']   = "Activo";
$lang['user_usertypeID'] = "ID de tipo de usuario";
$lang['user_username'] = "Nombre de usuario";
$lang['user_password'] = "Contraseña";
$lang['user_role'] 	= "Papel";
$lang['user_status'] = "Estado";
$lang['user_active'] = "Activar";
$lang['user_deactive'] = "Desactivar";
$lang['user_action'] = "Acción";
$lang['user_update'] = "Actualizar";
$lang['user_profile'] = "Perfil";
$lang['user_change_password'] = "Cambiar la contraseña";
$lang['user_new_password'] = "Nueva contraseña";
$lang['user_confirm_password'] = "confirmar Contraseña";

?>