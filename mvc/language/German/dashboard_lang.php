<?php 

$lang['dashboard_dashboard']     = "Instrumententafel";
$lang['dashboard_more']  = "Mehr Info";
$lang['dashboard_member']     = "Mitglieder";
$lang['dashboard_book'] 	  = "Bücher";
$lang['dashboard_categories'] 	  = "Kategorien";
$lang['dashboard_membership'] = "Mitgliedschaft";
$lang['dashboard_issued'] = "Problematisch";
$lang['dashboard_wastage'] = "Verschwendung";
$lang['dashboard_publication'] = "Veröffentlichung";
$lang['dashboard_writer'] = "der Autor";
$lang['dashboard_bar_chat'] = "Ausgestelltes und zurückgegebenes Balkendiagramm";
$lang['dashboard_issued_book'] = "Die letzten 5 veröffentlichten Bücher";
$lang['dashboard_returned_book'] = "Letzte 5 zurückgegebene Bücher";
$lang['circulation_member_name'] = "Mitgliedsname";
$lang['circulation_book_name'] = "Buchname";
$lang['circulation_writer_name'] = "Autorname";
$lang['circulation_issue_date'] = "Ausgabedatum";
$lang['circulation_expiry_date'] = "Letztes Rückreisedatum";
$lang['circulation_return_date'] = "Rückflugdatum";
$lang['categories_code'] = "Code";
$lang['categories_name'] = "Name";

?>