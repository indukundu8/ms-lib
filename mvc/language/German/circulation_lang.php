<?php 

$lang['circulation_circulation']     = "Verkehr";
$lang['circulation_add_circulation']     = "Kategorie hinzufügen";
$lang['circulation_issue_and_return']     = "Ausgestellt und zurückgegeben";
$lang['circulation_list']     = "Aufführen";

$lang['circulation_member_code'] = "Mitgliedscode";
$lang['circulation_member_id'] = "Mitgliedsnummer";
$lang['circulation_member_name'] = "Mitgliedsname";
$lang['circulation_book_name'] = "Buchname";
$lang['circulation_book_code'] = "Buchcode";
$lang['circulation_book_code_tooltip'] = "Geben Sie Ihren Buchcode ein";
$lang['circulation_writer_name'] = "Autorname";
$lang['circulation_issue_date'] = "Ausgabedatum";
$lang['circulation_expiry_date'] = "Letztes Datum der Rückkehr";
$lang['circulation_return_date'] = "Rückflugdatum";
$lang['circulation_return_status'] = "Rückgabestatus";
$lang['circulation_penalty'] = "Elfmeter";
$lang['circulation_penalty_fee'] = "Bußgeld";
$lang['circulation_no_of_days'] = "Anzahl der Tage";
$lang['circulation_penalty_message'] = "Das Rückgabedatum dieses Buches wurde überschritten. Bitte zahlen Sie die Strafe.";
$lang['circulation_penalty_note'] = "Hinweis";
$lang['member_penalty_amount'] = "Gesamtmenge";
$lang['circulation_action'] 	  = "Aktion";


$lang['circulation_insert'] = "Einfügen";
$lang['circulation_update'] = "Aktualisieren";
$lang['circulation_search'] = "Suche";
$lang['circulation_details'] = "Einzelheiten";
$lang['circulation_issue'] = "Ausgabe Buch";
$lang['circulation_issued'] = "die Ausgabe";
$lang['circulation_not_available'] = "Nicht verfügbar";
$lang['circulation_member_search'] = "Mitglied suchen";

$lang['circulation_member_gender'] = "Geschlecht";
$lang['circulation_member_occupation'] = "Besetzung";
$lang['circulation_member_phone'] = "Telefon";
$lang['circulation_book_return'] = "Rückkehr";
$lang['circulation_book_renewal'] = "Erneuerung";
$lang['circulation_book_lost'] = "verloren";
$lang['circulation_book_lost_message'] = "Da Sie das Buch verloren haben, müssen Sie den Preis für das Buch bezahlen.";
$lang['circulation_book_price'] = "Buchpreis";
$lang['circulation_book_payable_amount'] = "Zu zahlender Betrag";

$lang['book_photo'] = "Foto";
$lang['book_code'] = "Code";
$lang['book_name'] = "Name";
$lang['book_writer'] = "der Autor";
$lang['book_edition'] = "Auflage";
$lang['book_price'] = "Preis";
$lang['book_availability'] = "Verfügbarkeit";
$lang['book_rack_no'] = "Racknummer";
$lang['book_action'] = "Aktion";

?>