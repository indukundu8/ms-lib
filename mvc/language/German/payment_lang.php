<?php 

$lang['payment_payment']     	= "Zahlung";
$lang['payment_add_payment']    = "Zahlung hinzufügen";
$lang['payment_list']     		= "Aufführen";
$lang['payment_member_name']    = "Mitgliedsname";
$lang['payment_for']     		= "Zahlung für";
$lang['payment_amount']     	= "Zu zahlender Betrag";
$lang['payment_date']     		= "Datum";
$lang['payment_status']     	= "Status";
$lang['payment_action'] 	  	= "Aktion";
$lang['payment_receipt'] 	  	= "Kassenbon";
$lang['payment_money_receipt'] 	= "Geldeingang";
$lang['payment_email'] 			= "Email";
$lang['payment_phone'] 			= "Kontakt Nummer";
$lang['payment_mr'] 			= "HERR";
$lang['payment_from'] 	  		= "Bargeld erhalten von Cash";
$lang['payment_of'] 	  		= "Von";
$lang['payment_for'] 	  		= "Zum";
$lang['payment_inword'] 	  	= "In Worten";
$lang['payment_received_by'] 	= "Empfangen von";

?>