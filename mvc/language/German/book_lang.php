<?php 

$lang['book_book']     			= "Buch";
$lang['book_add_book']     		= "Buch hinzufügen";
$lang['book_update']     		= "Aktualisieren";
$lang['book_add']     			= "Hinzufügen";
$lang['book_please_select']     = "Bitte auswählen";
$lang['book_info']     			= "Buchinfo";
$lang['book_list']     			= "Aufführen";
$lang['book_name']     			= "Name";
$lang['book_categories']     	= "Kategorien";
$lang['book_publication']     	= "die Veröffentlichung";
$lang['book_isbn']     			= "ISBN";
$lang['book_writer']     		= "der Schriftsteller";
$lang['book_edition']     		= "Auflage";
$lang['book_edition_year']     	= "Ausgabejahr";
$lang['book_photo']     		= "Foto";
$lang['book_price']     		= "Preis";
$lang['book_quantity'] 			= "Menge";
$lang['book_availability'] 		= "Verfügbarkeit";
$lang['book_rack_no']    		= "Rack-Nummer";
$lang['book_purchase_price']    = "Kaufpreis";

$lang['book_details']     		= "Einzelheiten";
$lang['book_entry']     		= "Eintrag";


$lang['book_code']     			= "Code";
$lang['book_status']     		= "Status";
$lang['book_action'] 			= "Aktion";
$lang['book_code_tooltip']		= "Sie haben die Möglichkeit, Ihren eigenen Code für Ihre Bibliothek zu verwenden";
$lang['book_issued_quantity_tooltip']		= "Anzahl der für Mitglieder auszugebenden Bücher";
$lang['book_issued_quantity']		= "Anzahl der ausgegebenen Bücher";
$lang['isbn_tooltip']			= "Internationale Standardbuchnummer";


?>