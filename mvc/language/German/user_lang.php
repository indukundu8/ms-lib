<?php 

$lang['user_user']     = "Benutzer";
$lang['user_add_user']     = "Nutzer hinzufügen";
$lang['user_add']     = "Hinzufügen";
$lang['user_list']     = "Aufführen";
$lang['user_name']     = "Name";
$lang['user_dob']      = "Geburtsdatum";
$lang['user_gender']   = "Geschlecht";
$lang['user_please_select']   = "Bitte auswählen";
$lang['user_male']   = "Männlich";
$lang['user_female']   = "Weiblich";
$lang['user_religion'] = "Religion";
$lang['user_email']    = "Email";
$lang['user_phone']    = "Telefon";
$lang['user_address']  = "Adresse";
$lang['user_jod']      = "Eintrittsdatum";
$lang['user_photo']    = "Foto";
$lang['user_active']   = "Aktiv";
$lang['user_usertypeID'] = "Benutzertyp-ID";
$lang['user_username'] = "Nutzername";
$lang['user_password'] = "Passwort";
$lang['user_role'] 	= "Rolle";
$lang['user_status'] = "Status";
$lang['user_active'] = "aktivieren Sie";
$lang['user_deactive'] = "Deaktivieren";
$lang['user_action'] = "Aktion";
$lang['user_update'] = "Aktualisieren";
$lang['user_profile'] = "Profil";
$lang['user_change_password'] = "Kennwort ändern";
$lang['user_new_password'] = "Neues Kennwort";
$lang['user_confirm_password'] = "Kennwort bestätigen";

?>