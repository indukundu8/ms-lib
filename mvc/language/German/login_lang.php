<?php 

$lang['login_username']    = "Nutzername";
$lang['login_password']    = "Passwort";
$lang['login_sign_in']     = "Einloggen";
$lang['retry']     = "Anmeldung wiederholen";
$lang['login_remember_me'] = "Login-Daten speichern";
$lang['forgot_password'] = "Ich habe mein Passwort vergessen";
$lang['forgot_password_header'] = "Passwort vergessen";
$lang['forgot_password_sms'] = "Setzen Sie ihr Passwort hier zurück";
$lang['forgot_email_address'] = "E-Mail-Addresse";
$lang['reset_password'] = "Passwort zurücksetzen";
$lang['reset_password_sms'] = "Legen Sie Ihr neues Passwort fest";
$lang['new_password'] = "Neues Kennwort";
$lang['confirm_new_password'] = "Bestätige neues Passwort";
$lang['validation'] = "Überprüfungsprozess";
$lang['validation_code'] = "Verifizierungs-Schlüssel";
$lang['validation_sms'] = "Bitte kopiere den Validierungscode aus der E-Mail und füge ihn hier ein";
$lang['verify_button'] = "Verifizieren";
$lang['check_validation_code'] = "Validierungscode ist ungültig";

?>