<?php 
$lang['add']      = "Lägg till";
$lang['edit']     = "Redigera";
$lang['view']     = "Se";
$lang['delete']   = "Radera";

$lang['menu_dashboard']     = "instrumentbräda";
$lang['menu_user']          = "Användare";
$lang['menu_newuser']          = "Ny användare";
$lang['menu_usertype']      = "Användarroll";

$lang['menu_permissions']   = "Behörigheter";
$lang['menu_permissionlog'] = "Behörighetslogg";
$lang['menu_permissionmodule'] = "Tillståndsmodul";
$lang['menu_menu']             = "Meny";

$lang['menu_settings']  = "inställningar";
$lang['menu_sitesettings']  = "företagsprofil";
$lang['menu_membership']  = "Medlemskap";
$lang['menu_member']  = "Medlem";
$lang['menu_memberlist']  = "Medlemslista";
$lang['menu_categories']  = "Kategorier";
$lang['menu_book']  = "bok";
$lang['menu_book_archive']  = "Arkivera";
$lang['menu_wastage']  = "Slöseri";
$lang['menu_publication']  = "Offentliggörande";
$lang['menu_circulation']  = "Omlopp";
$lang['menu_payment']  = "Betalning";
$lang['menu_report']  = "Rapportera";
$lang['menu_writer']  = "Författare";
$lang['menu_emailsetting']  = "E-postkonfiguration";
$lang['menu_bookrequest']  = "Bokförfrågan";
$lang['menu_genarateid']  = "Genarate ID-kort";
$lang['menu_import']  = "Importera CSV";
?>