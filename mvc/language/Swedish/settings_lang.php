<?php 

$lang['settings_settings']     = "inställningar";
$lang['settings_general_setting']     = "Allmänna Inställningar";
$lang['settings_list']     = "Lista";

$lang['settings_company_name']      = "Företagsnamn";
$lang['settings_tag_line']      = "Tagline";
$lang['settings_type']      = "Affärs Typ";
$lang['settings_owner']      = "Ägarnamn";
$lang['settings_mobile']      = "Mobilnummer.";
$lang['settings_phone']      = "Telefonnr.";
$lang['settings_fax']      = "Faxnummer";
$lang['settings_email']      = "E-post";
$lang['settings_tax']      = "Skattenummer";
$lang['settings_address']      = "Adress";
$lang['settings_time_zone']      = "Tidszon";
$lang['settings_logo']      = "Företagslogotyp";
$lang['settings_currency_code']  = "Valutakod";
$lang['settings_currency_symbol']  = "Valutasymbol";
$lang['settings_prefixes']  = "Prefix";
$lang['category_prefixe']  = "Kategori";
$lang['member_prefixe']  = "Medlem";
$lang['book_prefixe']  = "bok";
$lang['membership_prefixe']  = "Medlemskap";
$lang['settings_language']  = "språk";

$lang['settings_update']     = "Uppdatering";
$lang['settings_action'] 	  = "Handling";

?>