<?php 

$lang['publication_publication']     = "Offentliggörande";
$lang['publication_add_publication']     = "Lägg till publikation";
$lang['publication_list']     = "Lista";

$lang['publication_name']       = "namn";
$lang['publication_note']  	  = "Notera";
$lang['publication_status']     = "Status";
$lang['publication_action'] 	  = "Handling";

$lang['publication_insert'] = "Föra in";
$lang['publication_update'] = "Uppdatering";

?>