<?php 

$lang['login_username']    = "Användarnamn";
$lang['login_password']    = "Lösenord";
$lang['login_sign_in']     = "Logga in";
$lang['retry']     = "Försök igen";
$lang['login_remember_me'] = "Spara inloggningsinformation";
$lang['forgot_password'] = "Jag har glömt mitt lösenord";
$lang['forgot_password_header'] = "Glömt ditt lösenord";
$lang['forgot_password_sms'] = "Återställ ditt lösenord här";
$lang['forgot_email_address'] = "E-postadress";
$lang['reset_password'] = "Återställ lösenord";
$lang['reset_password_sms'] = "Ange ditt nya lösenord";
$lang['new_password'] = "nytt lösenord";
$lang['confirm_new_password'] = "Bekräfta nytt lösenord";
$lang['validation'] = "Verifieringsprocess";
$lang['validation_code'] = "verifierings kod";
$lang['validation_sms'] = "Kopiera valideringskoden från e-postmeddelandet och klistra in den här";
$lang['verify_button'] = "Kontrollera";
$lang['check_validation_code'] = "Valideringskoden är ogiltig";

?>