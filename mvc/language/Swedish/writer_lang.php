<?php 

$lang['writer_writer']     = "Författare";
$lang['writer_add_writer']     = "Lägg till författare";
$lang['writer_list']     = "Lista";

$lang['writer_name']       = "namn";
$lang['writer_note']  	  = "Notera";
$lang['writer_status']     = "Status";
$lang['writer_action'] 	  = "Handling";

$lang['writer_insert'] = "Föra in";
$lang['writer_update'] = "Uppdatering";

?>