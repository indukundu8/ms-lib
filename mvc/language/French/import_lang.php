<?php 

$lang['import_import']     			= "Importer un fichier CSV";
$lang['import_list']     			= "Importer un fichier au format CSV";
$lang['import_note']     			= "Notes IMPORTANTES";
$lang['download']     				= "Télécharger";
$lang['select_file']     			= "Sélectionnez le fichier CSV";
$lang['upload_btn']     			= "Importer un fichier CSV";
$lang['import_note_1']     			= "Télécharger le format de fichier CSV";
$lang['import_note_2']     			= "Le titre du tableau du fichier CSV ne peut pas être modifié";
$lang['import_note_3']     			= "Utiliser correctement le préfixe du code du livre";
$lang['import_note_4']     			= "L'ID de catégorie de livre doit être correct";
$lang['import_note_5']     			= "L'ID de l'auteur du livre doit être correct";
$lang['import_note_6']     			= "L'identifiant des publications du livre doit être correct.";

?>