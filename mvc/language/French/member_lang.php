<?php 
$lang['member_member']     			= "Membre";
$lang['member_add_member']     		= "Ajouter un membre";
$lang['member_profile']     		= "Profil du membre";
$lang['member_update']     			= "Mettre à jour";
$lang['member_add']     			= "Ajouter";
$lang['member_please_select']     	= "Veuillez sélectionner";
$lang['member_info']     			= "Informations sur les membres";
$lang['member_list']     			= "Lister";
$lang['member_name']     			= "Nom";
$lang['member_father_name']     	= "Nom du père";
$lang['member_mother_name']     	= "Nom de la mère";
$lang['member_date_of_birth']     	= "Date de naissance";
$lang['member_nid_no']     			= "carte d'identité nationale";
$lang['member_nid_no_tooltip']     	= "numéro d'identification national";
$lang['member_membership']     		= "Adhésion";
$lang['member_membership_tooltip']	= "Sélectionnez le type d'adhésion";
$lang['member_occupation']     		= "Occupation";
$lang['member_parents']     		= "Parents";
$lang['member_gender']     			= "Genre";
$lang['member_male']     			= "Mâle";
$lang['member_female']     			= "femelle";
$lang['member_third_gender']     	= "Autres";
$lang['member_blood_group']     	= "Groupe sanguin";
$lang['member_religion']     		= "Religion";
$lang['member_phone']     			= "Numéro de contact";
$lang['member_email']     			= "E-mail";
$lang['member_since']     			= "Date d'enregistrement";
$lang['member_address']     		= "Adresse";
$lang['member_photo']     			= "photo";
$lang['member_change_password']     = "Changer le mot de passe";
$lang['member_new_password']     	= "nouveau mot de passe";
$lang['member_confirm_password']    = "Confirmez le mot de passe";
$lang['member_login_info']     		= "Informations de connexion";
$lang['member_username']     		= "Nom d'utilisateur";
$lang['member_password']     		= "Mot de passe";
$lang['member_confirm_password']    = "Confirmez le mot de passe";
$lang['member_code']     			= "Code";
$lang['member_status']     			= "Statut";
$lang['member_action'] 				= "action";
$lang['member_details_list'] 		= "Des détails";
$lang['member_circulation']			= "Détails de la diffusion";
$lang['member_payment_details']		= "Détails de paiement";

$lang['circulation_book_code']		= "Code du livre";
$lang['circulation_book_name']		= "Nom du livre";
$lang['circulation_writer_name']	= "Nom de l'écrivain";
$lang['circulation_issue_date']		= "Date de publication";
$lang['circulation_expiry_date']	= "Dernière date de retour";
$lang['circulation_return_date']	= "Date de retour";
$lang['circulation_penalty']		= "Peine";
$lang['circulation_return_status']	= "Statut";

$lang['payment_for']				= "Paiement pour";
$lang['payment_amount']				= "la somme";
$lang['payment_date']				= "Date";



?>