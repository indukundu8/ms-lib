<?php 
$lang['reports_reports']     	= "Rapports";
$lang['reports_member']     	= "Rapport de membre";
$lang['reports_book']     		= "Rapport de livre";
$lang['reports_payment']     	= "Rapport de paiement";
$lang['reports_categories']     = "Catégories Rapport";
$lang['reports_details']     	= "Détails ici";
$lang['reports_date']     		= "Date";
$lang['report_print']     		= "Imprimer";
$lang['report_generated']     	= "Générer un rapport";
$lang['report_generate']     	= "produire";
$lang['report_status']     		= "Statut";

$lang['member_name']     		= "Nom";
$lang['member_type']     		= "Taper";
$lang['member_occupation']     	= "Occupation";
$lang['member_parents']     	= "Parents";
$lang['member_phone']     		= "Numéro de contact";
$lang['member_address']     	= "Adresse";
$lang['report_photo']     		= "photo";
$lang['report_code']     		= "Code";
$lang['member_nid']     		= "Numéro d'identification national";
$lang['report_remark'] 			= "Remarque";

$lang['reports_please_select']     = "Veuillez sélectionner";
$lang['reports_membership_type']   = "Type d'adhésion";
$lang['reports_member_Active']     = "Actif";
$lang['reports_member_Inactive']   = "Inactif";

$lang['reports_categories_type']   = "Catégories";

$lang['book_name']     			= "Nom";
$lang['book_writer']     		= "l' auteur";
$lang['book_edition']     		= "Édition";
$lang['book_quantity'] 			= "Quantité";
$lang['book_availability'] 		= "Disponibilité";
$lang['book_rack_no']    		= "Numéro de rack";
$lang['categories_name']     = "Nom";
$lang['categories_book']     = "Livres";

$lang['report_payment']     = "Rapport de paiement";
$lang['reports_from_date']     = "Partir de la date";
$lang['reports_to_date']     = "À ce jour";
$lang['reports_for']     = "Pour";
$lang['reports_lost_book']     = "Frais de livre perdu";
$lang['reports_expiry_date']     = "Le temps de retour dépasse les frais";
$lang['reports_membership_fee']     = "Cotisation";
$lang['payment_member_name']    = "Nom de membre";
$lang['payment_for']     		= "Paiement pour";
$lang['payment_amount']     	= "Montant payable";
$lang['payment_date']     		= "Date";

?>