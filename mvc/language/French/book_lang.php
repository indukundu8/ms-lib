<?php 

$lang['book_book']     			= "Livre";
$lang['book_add_book']     		= "Ajouter un livre";
$lang['book_update']     		= "Mettre à jour";
$lang['book_add']     			= "Ajouter";
$lang['book_please_select']     = "Veuillez sélectionner";
$lang['book_info']     			= "Informations sur le livre";
$lang['book_list']     			= "Lister";
$lang['book_name']     			= "Nom";
$lang['book_categories']     	= "Catégories";
$lang['book_publication']     	= "Publication";
$lang['book_isbn']     			= "ISBN";
$lang['book_writer']     		= "l' écrivain";
$lang['book_edition']     		= "Édition";
$lang['book_edition_year']     	= "Année d'édition";
$lang['book_photo']     		= "photo";
$lang['book_price']     		= "Prix";
$lang['book_quantity'] 			= "Quantité";
$lang['book_availability'] 		= "Disponibilité";
$lang['book_rack_no']    		= "Numéro de rack";
$lang['book_purchase_price']    = "Prix ​​d'achat";

$lang['book_details']     		= "Des détails";
$lang['book_entry']     		= "Entrée";


$lang['book_code']     			= "Code";
$lang['book_status']     		= "Statut";
$lang['book_action'] 			= "action";
$lang['book_code_tooltip']		= "Vous avez la possibilité d'utiliser votre propre code pour votre bibliothèque";
$lang['book_issued_quantity_tooltip']		= "Nombre de livres à émettre pour les membres";
$lang['book_issued_quantity']		= "Nombre de livres publiés";
$lang['isbn_tooltip']			= "Numéro international normalisé du livre";


?>