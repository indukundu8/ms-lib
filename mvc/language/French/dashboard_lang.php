<?php 

$lang['dashboard_dashboard']     = "Tableau de bord";
$lang['dashboard_more']  = "Plus d'informations";
$lang['dashboard_member']     = "Membres";
$lang['dashboard_book'] 	  = "Livres";
$lang['dashboard_categories'] 	  = "Catégories";
$lang['dashboard_membership'] = "Adhésion";
$lang['dashboard_issued'] = "Publié";
$lang['dashboard_wastage'] = "Gaspillage";
$lang['dashboard_publication'] = "Publication";
$lang['dashboard_writer'] = "l' auteur";
$lang['dashboard_bar_chat'] = "Graphique à barres émis et retourné";
$lang['dashboard_issued_book'] = "5 derniers livres publiés";
$lang['dashboard_returned_book'] = "5 derniers livres retournés";
$lang['circulation_member_name'] = "Nom de membre";
$lang['circulation_book_name'] = "Nom du livre";
$lang['circulation_writer_name'] = "Nom de l'écrivain";
$lang['circulation_issue_date'] = "Date d'émission";
$lang['circulation_expiry_date'] = "Dernière date de retour";
$lang['circulation_return_date'] = "Date de retour";
$lang['categories_code'] = "Code";
$lang['categories_name'] = "Nom";

?>