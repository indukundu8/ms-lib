$(document).on('keypress', '.only_number', function(e){
    var yourInput = $(this).val();
    var keyCode = e.which ? e.which : e.keyCode
    if (!(keyCode >= 48 && keyCode <= 57)) {
        return false;
    }
});


// delete script
$(document).on('click', '.delete', function(e){
  "use strict";
  var id = $(this).attr("id");
    swal({
      title: 'Are you sure?',
      text: "It will be deleted permanently!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!',
      showLoaderOnConfirm: true,

        preConfirm: function(){
          return new Promise(function(resolve){
            $.ajax({
                url: THEMEBASEURL+'testimonials/delete',
                type: 'POST',
                data: {
                  'id'  : id,
                  [CSRFNAME]  : CSRFHASH,
                },
                dataType: 'json'
            })
            .done(function(response){
              swal('Deleted!', response.message, response.status);
              setTimeout(function(){ window.location.href = THEMEBASEURL+'testimonials'; }, 2000);
            })
            .fail(function(){
                swal('Oops...', 'Something went wrong with You !', 'error');
            });
         });
      },
    allowOutsideClick: false        
    }); 
}); 



$(document).on('click', '.onoffswitch-small-checkbox', function(e){
  "use strict";
  var status = '';
  var id = 0;
  if($(this).prop('checked')){
    status = '1';
    id = $(this).parent().attr("id");
  } else {
    status = '0';
    id = $(this).parent().attr("id");
  }
  if((status != '' || status != null) && (id !='')) {
    $.ajax({
      type: 'POST',
      url: THEMEBASEURL+'testimonials/status',
      data: {
        'id'      : id,
        'status'    : status,
        [CSRFNAME]   : CSRFHASH,
      },

      dataType: "html",
      success: function(data){
        var response = jQuery.parseJSON(data);
        console.log(data);
        if(response.confirmation == 'Success'){
          swal({
            title: "Successfully Updated.",
            position: 'top-end',
            type: 'success',
            showConfirmButton: false,
            timer: 1600,
          });
        } 
      }
    });
  }
});