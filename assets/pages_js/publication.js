$(function() {
  $('#example1').DataTable({
    'pageLength':10,
  });
})

$(document).on('click', '.insert', function(e){
  "use strict";
  var publication_name    = $("#publication_name").val();
  var publication_note    = $("#publication_note").val();

  $('#error_publication_name').html('');
  $('.error-name').removeClass('has-error');
  $.ajax({
    dataType: 'json',
    type: 'POST',
    url: THEMEBASEURL+'publication/add',
    data: {
      'publication_name'   : publication_name,
      'publication_note'   : publication_note, 
       [CSRFNAME]           : CSRFHASH,
    },
    dataType: "html",
    success: function(data){
      var response = jQuery.parseJSON(data);
      console.log(data);
      if(response.confirmation == 'Success'){
        $('#insert').modal('hide');
        swal({
          title: "Successfull.",
          position: 'top-end',
          type: 'success',
          showConfirmButton: false,
          timer: 1600,
        });
        setTimeout(function(){ window.location.href = THEMEBASEURL+'publication'; }, 1800);
      }else{
        $('#error_publication_name').html(response.validations['publication_name']);
        $('.error-name').addClass('has-error');
      }
    }
  });
});

$(document).on('click', '.update', function(e){
  "use strict";
  var id = $(this).attr('id');
  if(id != 'NULL' || id != '') {
    dataType: "json",
    $.ajax({
      type: 'POST',
      url: THEMEBASEURL+'publication/retrive',
      data: {
        'id'      : id,
        [CSRFNAME]   : CSRFHASH,
      },
      dataType: "html",
      success: function(data) {
        var response = jQuery.parseJSON(data);
        console.log(response);
        if(response.confirmation == 'Success') {
          $('#publicationID').val(response.id);
          $('#publication_name_up').val(response.publication_name);
          $('#publication_note_up').val(response.publication_note);


          $('.updated').click(function(){
            var id                 = $('#publicationID').val();
            var publication_name    = $("#publication_name_up").val();
            var publication_note    = $("#publication_note_up").val();

            $('#error_publication_name_up').html('');
            $('.error-class').removeClass('has-error');
            $.ajax({
              dataType: 'json',
              type: 'POST',
              url: THEMEBASEURL+'publication/edit',
              data: {
                'id'  : id,
                'publication_name'  : publication_name,
                'publication_note'  : publication_note,
                [CSRFNAME]            : CSRFHASH,
              },
              dataType: "html",
              success: function(data){
                var response = jQuery.parseJSON(data);
                console.log(data);
                if(response.confirmation == 'Success'){
                  $('#update').modal('hide');
                  swal({
                    title: "Successfully Updated.",
                    position: 'top-end',
                    type: 'success',
                    showConfirmButton: false,
                    timer: 1600,
                  });
                  setTimeout(function(){ window.location.href = THEMEBASEURL+'publication'; }, 1800);
                }else{
                  $('#error_publication_name_up').html(response.validations['publication_name']);
                  $('.error-class').addClass('has-error');

                }
              }
            });
          });
        }
      }
    });
  }
});



// delete script
$(document).on('click', '.delete', function(e){
  "use strict";
  var id = $(this).attr("id");
  swal({
    title: 'Are you sure?',
    text: "It will be deleted permanently!",
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Yes, delete it!',
    showLoaderOnConfirm: true,

    preConfirm: function() {
      return new Promise(function(resolve) {
       $.ajax({
        url: THEMEBASEURL+'publication/delete',
        type: 'POST',
        data: {
          'id'       : id,
          [CSRFNAME]    : CSRFHASH,
        },
        dataType: 'json'
      })
       .done(function(response){
        swal('Deleted!', response.message, response.status);
        setTimeout(function(){ window.location.href = THEMEBASEURL+'publication'; }, 2000);
      })
       .fail(function(){
        swal('Oops...', 'Something went wrong with You !', 'error');
      });
     });
    },
    allowOutsideClick: false        
  }); 
});


// status script
$(document).on('click', '.onoffswitch-small-checkbox', function(e){
  "use strict";
  var status = '';
  var id = 0;
  if($(this).prop('checked')){
    status = '1';
    id = $(this).parent().attr("id");
  } else {
    status = '0';
    id = $(this).parent().attr("id");
  }
  if((status != '' || status != null) && (id !='')) {
    $.ajax({
      type: 'POST',
      url: THEMEBASEURL+'publication/status',
      data: {
        'id'      : id,
        'status'    : status,
        [CSRFNAME]   : CSRFHASH,
      },

      dataType: "html",
      success: function(data){
        var response = jQuery.parseJSON(data);
        console.log(data);
        if(response.confirmation == 'Success'){
          swal({
            title: "Successfully Updated.",
            position: 'top-end',
            type: 'success',
            showConfirmButton: false,
            timer: 1600,
          });
        } 
      }
    });
  }
});


